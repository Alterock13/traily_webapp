import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Subject} from 'rxjs';

@Injectable()
export class AuthServiceService {

  currentUser = {
    id: '',
    mail: '',
    role: ''
  };
  currentUserChange: Subject<any> = new Subject<boolean>();

  constructor() {
    this.currentUser = {
      id: localStorage.getItem('userID'),
      mail: localStorage.getItem('userMail'),
      role: localStorage.getItem('userRole')
    };
    this.currentUserChange.subscribe((value) => {
      this.currentUser = value;
    });
  }


  public isAuthenticated(): boolean {
    /*
    Check if a user is authenticated and returns a boolean
    */
    const jwtHelper: JwtHelperService = new JwtHelperService();

    // const token = localStorage.getItem('token')['accessToken'];
    const token = JSON.parse(localStorage.getItem('token'));
    if (token !== undefined && token !== null) {
      const accessToken = token.accessToken;
      // Check whether the token is expired and return
      return !jwtHelper.isTokenExpired(accessToken);
    } else {
      return false;
    }
  }

  public isAdmin(): boolean {
    /*
    Check if a user is an admin and returns a boolean
    */
    return this.currentUser.role === 'admin';
  }

  public isCoach(): boolean {
    /*
    Check if a user is a coach and returns a boolean
    */
    return this.currentUser.role === 'coach';
  }

  updateUser(userObject) {
    /*
    Update the user object with new informations
    */
    this.currentUserChange.next(userObject);
  }
}
