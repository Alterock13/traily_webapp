import {TestBed, getTestBed} from '@angular/core/testing';

import {AuthGuardService} from './auth-guard.service';
import {AuthServiceService} from './auth-service.service';
import {Router} from '@angular/router';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('AuthGuardService', () => {
  let injector: TestBed;
  let authService: AuthServiceService;
  let guard: AuthGuardService;
  const routeMock: any = {data: {role: 'trainee'}};
  const routerMock = {navigate: jasmine.createSpy('navigate')};

  beforeEach(() => TestBed.configureTestingModule({
    providers: [AuthGuardService, AuthServiceService, {provide: Router, useValue: routerMock}, {
      provide: routeMock,
      useValue: routeMock
    }],
    imports: [HttpClientTestingModule]
  }));
  beforeEach(() => {
    injector = getTestBed();
    authService = injector.get(AuthServiceService);
    guard = injector.get(AuthGuardService);
  });

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });

  it('should redirect an unauthenticated user to the login route', () => {
    expect(guard.canActivate(routeMock)).toEqual(false);
    expect(routerMock.navigate).toHaveBeenCalledWith(['login']);
  });

  it('should allow the authenticated user to access app', () => {
    spyOn(authService, 'isAuthenticated').and.returnValue(true);
    expect(guard.canActivate(routeMock)).toEqual(true);
  });
});
