import {toLonLat} from 'ol/proj';

export class Utils {


  static dateDiffInDays(begin, end) {
    const MilliPerDay = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(begin.getFullYear(), begin.getMonth(), begin.getDate());
    const utc2 = Date.UTC(end.getFullYear(), end.getMonth(), end.getDate());
    return Math.floor((utc2 - utc1) / MilliPerDay);
  }

  static boxFromCoordinates(feature, offset) {
    const startingPoint = toLonLat(feature.getGeometry().getCoordinates()[0]);
    // Position, decimal degrees
    const lat = startingPoint[1];
    const lon = startingPoint[0];

    // Earth’s radius, sphere
    const R = 6378137;

    // offsets in meters
    const dn = offset;
    const de = offset;

    // Coordinate offsets in radians
    const dLat = dn / R;
    const dLon = de / (R * Math.cos(Math.PI * lat / 180));

    // OffsetPosition, decimal degrees
    const nelat = lat + dLat * 180 / Math.PI;
    const nelon = lon + dLon * 180 / Math.PI;
    const swlat = lat - dLat * 180 / Math.PI;
    const swlon = lon - dLon * 180 / Math.PI;
    return [swlat, swlon, nelat, nelon];
  }
}
