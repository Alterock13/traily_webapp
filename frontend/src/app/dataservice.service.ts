import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  baseAPIUrl = 'https://localhost:3009/';
  baseStravaUrl = 'https://www.strava.com/api/v3/';
  apiKeyStrava = ''; // Used for the token for the Strava API. It would need to be updated though.
  headers = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
  });
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getSegments(...coords: number[]): Observable<any> {
    /*
    This method make a GET request to the Strava API to get the tracks in the box made by the 4 coordinates.
    */

    const url = `${this.baseStravaUrl}segments/explore?bounds=${coords[0]},${coords[1]},
    ${coords[2]},${coords[3]}&access_token=${this.apiKeyStrava}`;

    return this.http.get(url, {
      headers: this.headers,
      reportProgress: true,
      observe: 'events'
    }).pipe(
      catchError(this.handleError));
  }

  register(username: string, email: string, password: string): Observable<any> {
    /*
    This method make a POST request with username email and password to the backend as to create a new account
    */


    const url = this.baseAPIUrl + 'api/auth/register';
    const payload = new HttpParams().set('name', username).set('email', email).set('password', password);

    return this.http.post(url, payload, {
      headers: this.headers
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));
  }

  login(email: string, password: string): Observable<any> {
    /*
    This method make a POST request with email and password to the backend as to log the user
    */


    const url = this.baseAPIUrl + 'api/auth/login';
    const payload = new HttpParams().set('email', email).set('password', password);

    return this.http.post(url, payload.toString(), {
      headers: this.headers
    });
  }

  refreshToken(email: string): Observable<any> {
    /*
    This method make a POST request with email to refresh the accessToken regurlaly
    */


    const url = this.baseAPIUrl + 'api/auth/refresh-token';
    const refreshToken = JSON.parse(localStorage.getItem('token')).refreshToken;
    const payload = new HttpParams().set('email', email).set('refreshToken', refreshToken);

    return this.http.post(url, payload.toString(), {
      headers: this.headers
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));
  }

  logout(userid: string): Observable<any> {
    /*
    This method make a POST request with userid to ask the backend to log out the user
    */


    const url = this.baseAPIUrl + 'api/auth/logout';
    const payload = new HttpParams().set('userId', userid);

    return this.http.post(url, payload.toString(), {
      headers: this.headers
    });
  }

  forgottenpassword(email: string): Observable<any> {
    /*
    This method make a POST request with userid to ask the backend for a temp password
    */


    const url = this.baseAPIUrl + 'api/auth/forgot-password';
    const payload = new HttpParams().set('email', email);
    return this.http.post(url, payload.toString(), {
      headers: this.headers
    });
  }

  loginFacebook(facebookToken) {
    /*
    This method make a POST request to the backend witht the token retrieved from the facebook api
    */


    const url = this.baseAPIUrl + 'api/auth/facebook';
    const payload = new HttpParams().set('access_token', facebookToken);

    return this.http.post(url, payload.toString(), {
      headers: this.headers
    });
  }

  getUsers(query ?: string): Observable<any> {
    /*
    This method make a GET request with an optional query parameter to retrieve user objects from the backend
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;
    if (query === undefined) {
      query = '';
    }
    const url = this.baseAPIUrl + 'api/users' + query;
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);
    return this.http.get(url, {
      headers: headersAuth
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));
  }


  getSingleUser(userid: string): Observable<any> {
    /*
    This method make a GET request with a userid to retrieve the object for a single user
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;

    const url = this.baseAPIUrl + 'api/users/' + userid;
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);
    return this.http.get(url, {
      headers: headersAuth
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));
  }

  editUser(userid: string, userinfo): Observable<any> {
    /*
    This method make a PATCH request to update the userid with a userinfo which is a json object
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;
    const url = this.baseAPIUrl + 'api/users/' + userid;
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);
    const payload = new HttpParams({
      fromObject: userinfo
    });

    return this.http.patch(url, payload.toString(), {
      headers: headersAuth
    });
  }

  deleteUser(userid): Observable<any> {
    /*
    This method make a DELETE request to ask the backend to delete a userid
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;

    const url = this.baseAPIUrl + 'api/users/' + userid;
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);

    return this.http.delete(url, {
      headers: headersAuth
    });
  }


  getTrainees(): Observable<any> {
    /*
    This method make a GET request to retrieve the trainees of a coach
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;
    const userid = localStorage.getItem('userID');
    const url = this.baseAPIUrl + 'api/users/' + userid + '/trainees';
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);
    return this.http.get(url, {
      headers: headersAuth
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));

  }

  assignTrainee(coachid, traineemail) {
    /*
    This method make a POST request to assign a trainee with traineemail to a coach with coachid
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;

    const url = this.baseAPIUrl + 'api/users/' + coachid + '/trainees';
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);
    const payload = new HttpParams().set('email', traineemail);
    return this.http.post(url, payload, {
      headers: headersAuth
    });
  }

  removeTrainee(coachid, userid): Observable<any> {
    /*
    This method make a DELETE request to remove a trainee from a coach
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;

    const url = this.baseAPIUrl + 'api/users/' + coachid + '/trainees';
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);
    const payload = new HttpParams().set('traineeId', userid);
    const httpOptions = {
      headers: headersAuth,
      body: payload,
    };
    return this.http.delete(url, httpOptions);
  }


  updateRatingTrack(userid, trackid, rating): Observable<any> {
    /*
    This method make a PATCH request to update the rating of a track with trackid from a user with userid
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;
    const url = this.baseAPIUrl + 'api/users/' + userid + '/tracks/' + trackid;
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);
    const payload = new HttpParams().set('rating', rating);
    return this.http.patch(url, payload.toString(), {
      headers: headersAuth
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));

  }

  getUserTracks(userid): Observable<any> {
    /*
    This method make a GET request to retrieve the tracks of a user
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;

    const url = this.baseAPIUrl + 'api/users/' + userid + '/tracks';
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);
    return this.http.get(url, {
      headers: headersAuth
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));
  }

  createUserTrack(track): Observable<any> {
    /*
    This method make a POST request to create a track for a user
    */


    const userID = localStorage.getItem('userID');
    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);

    const url = this.baseAPIUrl + 'api/users/' + userID + '/tracks';
    const payload = new HttpParams().set('user', userID).set('name', track.name)
      .set('duration', track.duration).set('locations', JSON.stringify(track.locations));


    return this.http.post(url, payload.toString(), {
      headers: headersAuth
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));
  }

  getImage(userid): Observable<any> {
    /*
    This method make a GET request to retrieve the image from a user with userid
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;
    const lightHeaders = new HttpHeaders();
    const headersAuth = lightHeaders.set('Authorization', `Bearer ${accessToken}`);

    const url = this.baseAPIUrl + 'api/users/' + userid + '/image';
    return this.http.get(url, {
      headers: headersAuth,
      responseType: 'blob',
    });
  }

  uploadImage(userid, file): Observable<any> {
    /*
    This method make a POST request to post a new image to the backend
    */

    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;
    const lightHeaders = new HttpHeaders();
    const headersAuth = lightHeaders.set('Authorization', `Bearer ${accessToken}`);

    const url = this.baseAPIUrl + 'api/users/' + userid + '/image';
    const payload = new FormData();
    payload.append('file', file);
    return this.http.post(url, payload, {
      headers: headersAuth,
      responseType: 'blob',
    });
  }

  deleteUserTrack(track): Observable<any> {
    /*
    This method make a DELETE request to delete the track from a user
    */

    const userID = localStorage.getItem('userID');
    const accessToken = JSON.parse(localStorage.getItem('token')).accessToken;
    const headersAuth = this.headers.set('Authorization', `Bearer ${accessToken}`);

    const url = this.baseAPIUrl + 'api/users/' + userID + '/tracks/' + track;
    return this.http.delete(url, {
      headers: headersAuth
    }).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError));
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
