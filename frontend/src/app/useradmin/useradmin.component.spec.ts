import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UseradminComponent} from './useradmin.component';
import {
  MAT_DIALOG_DATA,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDialog,
  MatDialogModule,
  MatDialogRef,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBar,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {UsersettingsComponent} from '../usersettings/usersettings.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ImageCropperModule} from 'ngx-image-cropper';
import {RouterTestingModule} from '@angular/router/testing';
import {appRoutes} from '../app.routes';
import {LoginComponent} from '../login/login.component';
import {MapComponent} from '../map/map.component';
import {RatingComponent} from '../map/rating/rating.component';
import {AuthGuardService} from '../auth-guard.service';
import {AuthServiceService} from '../auth-service.service';
import {ServerErrorInterceptor} from '../RequestInterceptor';

describe('UseradminComponent', () => {
  let component: UseradminComponent;
  let fixture: ComponentFixture<UseradminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UseradminComponent, LoginComponent, MapComponent, UsersettingsComponent, RatingComponent],
      imports: [ // Anything that uses the Materials lib needs to import in order to test
        BrowserModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatSidenavModule,
        BrowserAnimationsModule,
        MatExpansionModule,
        FlexLayoutModule,
        MatDialogModule,
        MatFormFieldModule,
        MatMenuModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        HttpClientModule,
        MatProgressBarModule,
        MatSnackBarModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSelectModule,
        ImageCropperModule,
        RouterTestingModule.withRoutes(appRoutes)
      ],
      providers: [AuthGuardService, AuthServiceService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ServerErrorInterceptor,
          multi: true
        }, {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        },
        {
          provide: MatDialogRef,
          useValue: {}
        },
        {
          provide: MatDialog,
          useValue: {}
        },
        {
          provide: MatSnackBar,
          useValue: {}
        },
      ]

    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(UseradminComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
