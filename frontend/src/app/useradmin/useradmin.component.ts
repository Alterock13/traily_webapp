import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {DataServiceService} from '../dataservice.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {AuthServiceService} from '../auth-service.service';

@Component({
  selector: 'app-useradmin',
  templateUrl: './useradmin.component.html',
  styleUrls: ['./useradmin.component.scss']
})
export class UseradminComponent implements OnInit, AfterViewInit {

  dataService: DataServiceService;
  users = [];
  displayedColumns: string[] = [];
  currentUser = {
    id: '',
    mail: '',
    role: ''
  };
  traineemail = '';
  message = {
    text: '',
    type: ''
  };

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(dataService: DataServiceService, private router: Router, public auth: AuthServiceService) {
    this.dataService = dataService;
  }

  ngOnInit() {
    this.currentUser = {
      id: localStorage.getItem('userID'),
      mail: localStorage.getItem('userMail'),
      role: localStorage.getItem('userRole')
    };
    this.auth.currentUserChange.subscribe(updatedUser => {
      this.currentUser = updatedUser;
    });

    if (this.currentUser.role === 'admin') {
      this.displayedColumns = ['picture', 'name', 'email', 'role', 'delete'];
    } else if (this.currentUser.role === 'coach') {
      this.displayedColumns = ['picture', 'name', 'email', 'delete'];
    }
  }

  ngAfterViewInit() {
    if (this.currentUser.role === 'admin') {
      this.dataService.getUsers().subscribe(result => {
        this.users = result.data;
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.users.forEach(user => {
          const that = this;
          this.dataService.getImage(user.id).subscribe(blob => {
            let userPicture;
            const reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = () => {
              userPicture = reader.result;
              // tslint:disable-next-line: no-string-literal
              that.users[that.users.indexOf(user)]['picture'] = userPicture;
            };
          }, error => {
            that.users[that.users.indexOf(user)].picture = 'assets/img/Portrait_Default.png';
          });
        });
      });
    } else if (this.currentUser.role === 'coach') {
      this.dataService.getTrainees().subscribe(result => {
        this.users = result.data;
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.users.forEach(user => {
          const that = this;
          this.dataService.getImage(user.id).subscribe(blob => {
            let userPicture;
            const reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = () => {
              userPicture = reader.result;
              // tslint:disable-next-line: no-string-literal
              that.users[that.users.indexOf(user)]['picture'] = userPicture;
            };
          }, error => {
            that.users[that.users.indexOf(user)].picture = 'assets/img/Portrait_Default.png';
          });
        });
      });
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  assignTrainee() {
    this.dataService.assignTrainee(this.currentUser.id, this.traineemail).subscribe(result => {

        this.ngAfterViewInit();
        this.message = {
          // tslint:disable-next-line: no-string-literal
          text: `Trainee « ${result['name']} » has been assigned to you`,
          type: 'success'
        };
        setTimeout(() => {
          this.message = {
            text: '',
            type: ''
          };
        }, 5000); // Reset the message to empty
      },
      error => {

        this.message = {
          text: `There has been an error with the backend. It returned the error : ${error.error}`,
          type: 'error'
        };
        setTimeout(() => {
          this.message = {
            text: '',
            type: ''
          };
        }, 5000); // Reset the message to empty
      });
  }

  deleteUser(userid) {
    this.dataService.deleteUser(userid).subscribe(result => {

      this.ngAfterViewInit();
    });
  }

  removeTrainee(userid) {
    this.dataService.removeTrainee(this.currentUser.id, userid).subscribe(result => {

      this.ngAfterViewInit();
    });
  }


}
