import {HttpEventType} from '@angular/common/http';
import {AfterViewInit, Component, ElementRef, Inject, Input, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {decode} from 'google-polyline';
import * as NoSleep from 'nosleep.js';
import {Overlay} from 'ol';
import {getCenter} from 'ol/extent';
import Feature from 'ol/Feature';
import LineString from 'ol/geom/LineString';
import OlTileLayer from 'ol/layer/Tile';
import LayerVector from 'ol/layer/Vector';
import OlMap from 'ol/Map';
import {fromLonLat} from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import OlXYZ from 'ol/source/XYZ';
import {Fill, Stroke, Style} from 'ol/style.js';
import OlView from 'ol/View';
import {DataServiceService} from '../dataservice.service';
import {Utils} from '../utils';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {

  @ViewChild('container') container: ElementRef;
  @Input() drawerActivated = false;

  // For the rating component
  ratingClicked: number;
  itemIdRatingClicked: string;

  // User related info
  userid: string;
  loggedUserid: string;

  colors = ['#00FF00', '#FF0000', '#0000FF', '#A328C9'];


  // Geolocation
  startDateCurrentGeo: Date;
  currentUserRole;

  // Tracks
  tempTrack = {
    name: '',
    duration: 0,
    locations: [],
    createdAt: new Date(),
    distance: '',
    id: '',
  };
  trackList = [];
  initDone = false;

  geoID: number;
  geoTracking = 'stopped';
  noSleep = new NoSleep();


  interval;
  chrono = 0;

  query = false;

  // Opnelayers
  map: OlMap;
  source: OlXYZ;
  layer: OlTileLayer;
  view: OlView;
  vectorLayer: LayerVector;
  vectorLayerStrava: LayerVector;
  vectorSource: VectorSource;
  vectorSourceStrava: VectorSource;

  public dataService: DataServiceService;

  constructor(private router: Router, public dialog: MatDialog, dataService: DataServiceService,
              private snackBar: MatSnackBar, private route: ActivatedRoute) {
    this.dataService = dataService;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngAfterViewInit() {

    this.currentUserRole = localStorage.getItem('userRole');
    this.loggedUserid = localStorage.getItem('userID');
    this.userid = this.route.snapshot.params.userid;


    this.vectorSource = new VectorSource();
    this.vectorLayer = new LayerVector({
      source: this.vectorSource,
    });
    this.vectorSourceStrava = new VectorSource();
    this.vectorLayerStrava = new LayerVector({
      source: this.vectorSourceStrava,
    });
    this.source = new OlXYZ({
      url: 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png'
      // url: 'http://tile.osm.org/{z}/{x}/{y}.png' // More detailed but can be a bit overwhelmed with information sometimes
      // url: "https://a.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png" // DarkMode
      // url: "https://stamen-tiles-a.a.ssl.fastly.net/terrain/{z}/{x}/{y}.png" // Not enough zoom
    });

    this.layer = new OlTileLayer({
      source: this.source,
      preload: Infinity // Preload tiles for smoother animations
    });

    this.view = new OlView({
      center: fromLonLat([5.7245, 45.1885]),
      zoom: 5,
      maxZoom: 19,
    });


    this.map = new OlMap({
      target: 'map',
      layers: [this.layer, this.vectorLayer, this.vectorLayerStrava],
      view: this.view
    });

    this.showLabelOnHover();
    this.showStravaOnClick();

    this.dataService.getUserTracks(this.userid).subscribe(result => {
      result.data.forEach(track => {
        const color = this.colors[this.trackList.length % this.colors.length];
        const featureTrack = this.addPath(track.locations, track.name, color, track.duration);
        this.trackList.push([track, featureTrack]);

      });
      if (this.trackList.length > 0) {
        this.zoomOnTrack(this.trackList[this.trackList.length - 1][1]);
      }
      this.initDone = true;
    });


  }

  addPath(coords, trackName: string, lineColor: string, duration: number, rezoom: boolean = true) {


    for (let i = 0; i < coords.length; i++) {
      coords[i] = fromLonLat(coords[i]);
    }
    const newFeature = new Feature({
      geometry: new LineString(coords),
      label: trackName,
      duration,
    });

    const pathStyle = new Style({
      fill: new Fill({
        color: lineColor,
        weight: 4
      }),
      stroke: new Stroke({
        color: lineColor,
        width: 2
      }),
    });
    newFeature.setStyle(pathStyle);
    if (rezoom === true) {
      this.vectorSource.addFeature(newFeature);
      // We zoom on the last entry
      // this.zoomOnTrack(this.vectorSource.getFeatures()[this.vectorSource.getFeatures().length - 1])
    } else {
      this.vectorSourceStrava.addFeature(newFeature);
    }
    return newFeature;
  }


  geoloc() {
    if (this.geoTracking === 'launched') {

      this.noSleep.disable();
      navigator.geolocation.clearWatch(this.geoID);
      clearInterval(this.interval);
      this.startDateCurrentGeo = undefined;
      // Default name for tracks
      this.tempTrack.name = 'Track_' + (this.trackList.length + 1).toString();
      this.tempTrack.duration = this.chrono;
      this.chrono = 0;
      this.openDialog();
      this.geoTracking = 'stopped';
      return;
    }
    if (navigator.geolocation) {
      this.geoTracking = 'waiting';
      this.noSleep.enable();
      this.geoID = navigator.geolocation.watchPosition(this.showPosition.bind(this), undefined, {
        enableHighAccuracy: true
      });


    } else {

    }
  }


  showPosition(position) {
    this.geoTracking = 'launched';
    if (this.startDateCurrentGeo === undefined) {
      this.startDateCurrentGeo = new Date();
      this.startChrono();
    }
    this.tempTrack.locations.push([position.coords.longitude, position.coords.latitude]);
  }

  counter(nbStars: string) {
    let i = 0;
    try {
      nbStars !== '' && nbStars !== undefined ? i = parseInt(nbStars) : i = 0;
    } catch (error) {

    }
    return new Array(i);
  }

  startChrono() {
    const intervalChrono = 1000;
    this.interval = setInterval(() => {
      this.chrono += intervalChrono;
    }, intervalChrono);
  }

  showLabelOnHover() {
    const overlay = new Overlay({
      element: this.container.nativeElement,
      autoPan: true,
      autoPanAnimation: {
        duration: 250
      }
    });
    this.map.addOverlay(overlay);
    const that = this;
    this.map.on('pointermove', function (evt) {
      let hasFeature = false;
      let featureObject;
      this.forEachFeatureAtPixel(evt.pixel, feature => {
        const coordinate = evt.coordinate;
        // Offset to not interfere with onclick event
        // coordinate[0] = coordinate[0] + 20
        // coordinate[1] = coordinate[1] - 100
        overlay.setPosition(coordinate);
        hasFeature = true;
        featureObject = feature;
      }, {
        hitTolerance: 10
      });
      if (!hasFeature) {
        that.container.nativeElement.className = 'hide';
      } else {
        that.container.nativeElement.className = 'show';
        (that.container.nativeElement as HTMLInputElement).innerHTML = featureObject.get('label');
      }
    });
  }

  showStravaOnClick() {
    const that = this;
    this.map.on('click', function (evt) {
      that.vectorSourceStrava.clear();
      this.forEachFeatureAtPixel(evt.pixel, feature => {
        const boxCoordinate = Utils.boxFromCoordinates(feature, 10000);
        that.RetrieveAndPlotStrava(...boxCoordinate);
      }, {
        hitTolerance: 10 // So we can click a bit off the line
      });
    });
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(DialogTrackNameComponent, {
      minWidth: '250px',
      data: this.tempTrack,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(result => {


      this.tempTrack.name = result;
      const color = this.colors[this.trackList.length % this.colors.length];
      this.dataService.createUserTrack(this.tempTrack).subscribe(newTrackResult => {

        const featureTrack = this.addPath(newTrackResult.locations, newTrackResult.name, color, newTrackResult.duration);
        this.tempTrack.createdAt = newTrackResult.createdAt;
        this.tempTrack.distance = newTrackResult.distance;
        this.tempTrack.id = newTrackResult.id;
        this.trackList.push([this.tempTrack, featureTrack]);
        this.zoomOnTrack(featureTrack);
        this.tempTrack = {
          id: '',
          name: '',
          duration: 0,
          locations: [],
          createdAt: new Date(),
          distance: '',
        };
      });

    });
  }

  deleteTrack(track) {


    this.dataService.deleteUserTrack(track[0].id).subscribe(() => {
      this.vectorSource.removeFeature(track[1]);
      this.trackList.splice(this.trackList.indexOf(track), 1);
    });
  }

  zoomOnTrack(track) {
    // We wait for the previous animation to end before starting for the current marker
    const tempVectorSource = new VectorSource();
    tempVectorSource.addFeature(track);


    const duration = 1400;
    const resolution = this.view.getResolutionForExtent(tempVectorSource.getExtent());
    const zoom = this.view.getZoomForResolution(resolution);
    const center = getCenter(tempVectorSource.getExtent());
    this.view.animate({
      center,
      duration
    });

    this.view.animate({
      zoom: zoom === 'Infinity' ? 10 : zoom - 8,
      duration: duration / 2
    }, {
      zoom: zoom === 'Infinity' ? 18 : zoom - 1,
      duration: duration / 2
    });
  }

  RetrieveAndPlotStrava(...coords: number[]) {

    const subscription = this.dataService.getSegments(...coords).subscribe(results => {
      this.query = true;
      if (results.type === HttpEventType.DownloadProgress) {

      } else if (results.type === HttpEventType.Response) {
        if (results.status === 200) {
          //
          results = results.body;
          results.segments.forEach(result => {

            const newCoords = decode(result.points);

            const coordsReverse = [];
            newCoords.forEach(element => {
              // element = element.map(element.pop,[...element]);
              coordsReverse.push(element.reverse());

            });
            this.addPath(coordsReverse, result.name, '#000000', 0, false);

          });
          this.query = false;
          // zoom on the extent
          if (this.map.getView().getAnimating() === true) {

            setTimeout(() => {
              this.map.getView().fit(this.vectorSourceStrava.getExtent(), {
                size: this.map.getSize(),
                maxZoom: 18,
                duration: 700
              });
            }, 1000);
          } else {
            this.map.getView().fit(this.vectorSourceStrava.getExtent(), {
              size: this.map.getSize(),
              maxZoom: 18,
              duration: 700
            });
          }
        }
      } else if (results.type === HttpEventType.ResponseHeader) {
        if (results.status !== 200) {

          this.query = false;
          this.snackBar.open('Couldn\'t complete. Code returned is : ' + results.status, 'Close', {
            duration: 5000,
          });
          subscription.unsubscribe();
          return;
        }
      }

    });
  }

  dateFilter(days, event) {

    event.target.color = 'warn';


    this.clearFilter();
    this.trackList.forEach(track => {
      const dateTrack = new Date(track[0].createdAt);
      const currentDate = new Date();
      if (Utils.dateDiffInDays(dateTrack, currentDate) > days) {

        this.vectorSource.removeFeature(track[1]);
      }
    });
  }

  clearFilter() {
    this.trackList.forEach(track => {
      if (!this.vectorSource.getFeatures().includes(track[1])) {
        try {
          this.vectorSource.addFeature(track[1]);
        } catch (error) {

        }
      }
    });
  }

  drawerToggle() {
    this.drawerActivated = !this.drawerActivated;
    const that = this;
    setTimeout(() => {
      that.map.updateSize();
    }, 10); // Delay apparently necessary
  }

  ratingComponentClick(clickObj: any): void {

    const item = this.trackList.find(((i: any) => i[0].id === clickObj.itemId));

    if (!!item) {
      item.rating = clickObj.rating;
      this.ratingClicked = clickObj.rating;

      this.dataService.updateRatingTrack(this.userid, item[0].id, clickObj.rating).subscribe(() => {

      });
    }

  }

}

@Component({
  selector: 'dialog-trackName',
  templateUrl: 'dialog-trackName.html',
})
export class DialogTrackNameComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogTrackNameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

}
