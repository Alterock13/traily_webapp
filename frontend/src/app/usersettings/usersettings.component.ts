import {Component, Inject, OnInit} from '@angular/core';
import {DataServiceService} from '../dataservice.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {AuthServiceService} from '../auth-service.service';
import {error} from 'selenium-webdriver';

@Component({
  selector: 'app-usersettings',
  templateUrl: './usersettings.component.html',
  styleUrls: ['./usersettings.component.scss']
})
export class UsersettingsComponent implements OnInit {

  public dataService: DataServiceService;
  message = {
    text: '',
    type: ''
  };
  myForm: FormGroup;
  userid;
  registerBool = false;
  roles = ['admin', 'coach', 'trainee'];
  coachList = [];
  currentUserRole: string;
  currentUserId: string;

  profilePicture;
  profilePictureFile;

  constructor(dataService: DataServiceService, private route: ActivatedRoute, public dialog: MatDialog, private auth: AuthServiceService) {
    this.dataService = dataService;
  }


  ngOnInit() {
    this.userid = this.route.snapshot.params.userid;
    this.currentUserRole = localStorage.getItem('userRole');
    this.currentUserId = localStorage.getItem('userID');
    // this.profilePicture = localStorage.getItem('userPicture');
    const that = this;
    this.dataService.getImage(this.userid).subscribe(blob => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        that.profilePicture = reader.result;
      };
    }, () => {
      that.profilePicture = 'assets/img/Portrait_Default.png';
    });
    this.myForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.maxLength(128)]),
      password: new FormControl(null, [Validators.minLength(6), Validators.maxLength(128)]),
      email: new FormControl(null, [Validators.required, Validators.email])
    });
    // Only if the user is admin
    if (this.currentUserRole === 'admin') {
      this.myForm.addControl('role', new FormControl(null, [Validators.required]));
      this.myForm.addControl('coach', new FormControl(null));
      this.dataService.getUsers('?role=coach').subscribe(result => {

        this.coachList = result.data;
      });
    }
    //
    this.dataService.getSingleUser(this.userid).subscribe(result => {

      this.myForm.patchValue({
        name: result.name,
        email: result.email,
        role: result.role,
        coach: this.coachList.find(coach => {
          return coach.id === result.coach;
        })
      });
    });
  }

  editUser() {
    const name = this.myForm.get('name').value;
    const email = this.myForm.get('email').value;
    const password = this.myForm.get('password').value;
    const user = {};
    if (this.currentUserRole === 'admin') {
      const role = this.myForm.get('role').value;
      const coach = this.myForm.get('coach').value.id;
      // tslint:disable-next-line: no-string-literal
      user['role'] = role;
      // tslint:disable-next-line: no-string-literal
      user['coach'] = coach;
    }
    if (password != null) {
      // tslint:disable-next-line: no-string-literal
      user['password'] = password;
    }
    // tslint:disable-next-line: no-string-literal
    user['name'] = name;
    // tslint:disable-next-line: no-string-literal
    user['email'] = email;

    this.dataService.editUser(this.userid, user).subscribe(result => {


      if (this.userid === this.currentUserId) {
        this.auth.updateUser({
          id: result.id,
          mail: result.email,
          role: result.role,
          image: this.profilePicture
        });
      }
      this.message = {
        text: 'Successful update',
        type: 'success'
      };
      setTimeout(() => {
        this.message = {
          text: '',
          type: ''
        };
      }, 5000); // Reset the message to empty
    }, err => {


      this.message = {
        text: `There has been an error with the backend. It returned the error code : ${err.status}`,
        type: 'error'
      };
      setTimeout(() => {
        this.message = {
          text: '',
          type: ''
        };
      }, 5000); // Reset the message to empty
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogImageLoaderComponent, {
      minWidth: '300px',
      data: this.profilePicture,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(imageloaded => {


      if (imageloaded !== undefined) {
        this.profilePicture = imageloaded.base64;
        this.profilePictureFile = imageloaded.file;
        const file = new File([imageloaded.file], this.userid, {
          type: imageloaded.file.type,
          lastModified: Date.now()
        });

        this.dataService.uploadImage(this.userid, file).subscribe(imageuploaded => {

          const reader = new FileReader();
          const that = this;
          reader.readAsDataURL(imageuploaded);
          reader.onloadend = () => {
            that.profilePicture = reader.result;
            localStorage.setItem('userPicture', that.profilePicture);
          };
          if (this.userid === this.currentUserId) {
            this.dataService.getSingleUser(this.userid).subscribe(userInfo => {
              this.auth.updateUser({
                id: userInfo.id,
                mail: userInfo.email,
                role: userInfo.role,
                image: this.profilePicture
              });
            });
          }
          this.message = {
            text: 'Successful image upload',
            type: 'success'
          };
          setTimeout(() => {
            this.message = {
              text: '',
              type: ''
            };
          }, 5000); // Reset the message to empty
        }, err => {


          this.message = {
            text: `There has been an error with the backend. It returned the error code : ${err.status}`,
            type: 'error'
          };
          setTimeout(() => {
            this.message = {
              text: '',
              type: ''
            };
          }, 5000); // Reset the message to empty
        });
      }
    });
  }

}


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-imageLoader',
  templateUrl: 'dialog-imageLoader.html',
})
export class DialogImageLoaderComponent {
  imageChangedEvent: any = '';
  croppedImage: any = '';


  constructor(
    public dialogRef: MatDialogRef<DialogImageLoaderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event;
  }

  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }

  loadImageFailed() {
    // show message
  }
}
