import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {MapComponent} from './map/map.component';
import {LoginComponent} from './login/login.component';
import {UsersettingsComponent} from './usersettings/usersettings.component';
import {
  MAT_DIALOG_DATA,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDialog,
  MatDialogModule,
  MatDialogRef,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBar,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {appRoutes} from './app.routes';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UseradminComponent} from './useradmin/useradmin.component';
import {AuthGuardService} from './auth-guard.service';
import {AuthServiceService} from './auth-service.service';
import {ServerErrorInterceptor} from './RequestInterceptor';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ImageCropperModule} from 'ngx-image-cropper';
import {RatingComponent} from './map/rating/rating.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MapComponent,
        LoginComponent,
        UsersettingsComponent,
        UseradminComponent,
        RatingComponent,
      ],
      imports: [
        BrowserModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatSidenavModule,
        BrowserAnimationsModule,
        MatExpansionModule,
        FlexLayoutModule,
        MatDialogModule,
        MatFormFieldModule,
        MatMenuModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        HttpClientModule,
        MatProgressBarModule,
        MatSnackBarModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSelectModule,
        ImageCropperModule,
        RouterTestingModule.withRoutes(appRoutes)
      ],
      providers: [AuthGuardService, AuthServiceService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ServerErrorInterceptor,
          multi: true
        }, {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        },
        {
          provide: MatDialogRef,
          useValue: {}
        },
        {
          provide: MatDialog,
          useValue: {}
        },
        {
          provide: MatSnackBar,
          useValue: {}
        },
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Traily'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Traily');
  });

  // it('should render title in a h1 tag', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to frontend!');
  // });
});
