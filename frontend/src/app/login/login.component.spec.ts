import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {
  MAT_DIALOG_DATA,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDialog,
  MatDialogModule,
  MatDialogRef,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBar,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';


import {LoginComponent} from './login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UsersettingsComponent} from '../usersettings/usersettings.component';
import {BrowserModule} from '@angular/platform-browser';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ImageCropperModule} from 'ngx-image-cropper';
import {RouterTestingModule} from '@angular/router/testing';
import {appRoutes} from '../app.routes';
import {MapComponent} from '../map/map.component';
import {UseradminComponent} from '../useradmin/useradmin.component';
import {RatingComponent} from '../map/rating/rating.component';
import {AuthGuardService} from '../auth-guard.service';
import {AuthServiceService} from '../auth-service.service';
import {ServerErrorInterceptor} from '../RequestInterceptor';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent, MapComponent, UsersettingsComponent, UseradminComponent, RatingComponent],
      imports: [ // Anything that uses the Materials lib needs to import in order to test
        BrowserModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatSidenavModule,
        BrowserAnimationsModule,
        MatExpansionModule,
        FlexLayoutModule,
        MatDialogModule,
        MatFormFieldModule,
        MatMenuModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        HttpClientModule,
        MatProgressBarModule,
        MatSnackBarModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSelectModule,
        ImageCropperModule,
        RouterTestingModule.withRoutes(appRoutes)
      ],
      providers: [AuthGuardService, AuthServiceService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ServerErrorInterceptor,
          multi: true
        }, {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        },
        {
          provide: MatDialogRef,
          useValue: {}
        },
        {
          provide: MatDialog,
          useValue: {}
        },
        {
          provide: MatSnackBar,
          useValue: {}
        },
      ]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
