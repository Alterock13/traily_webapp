import {Component, EventEmitter, NgZone, OnInit, Output} from '@angular/core';
import {DataServiceService} from '../dataservice.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthServiceService} from '../auth-service.service';

declare var FB: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public dataService: DataServiceService;
  message = {
    text: '',
    type: ''
  };
  @Output() userInfoEvent = new EventEmitter<any>();
  myForm: FormGroup;
  profilePicture;
  registerBool = false;

  constructor(dataService: DataServiceService, private router: Router, private route: ActivatedRoute,
              private auth: AuthServiceService, private zone: NgZone) {
    this.dataService = dataService;
  }

  ngOnInit() {
    if (this.auth.isAuthenticated() === true) {
      const userid = localStorage.getItem('userID');
      this.router.navigate(['/map', userid]);
    }
    this.route.queryParams.subscribe(params => {
      if (params.logout === 'true') {

        this.logout();
      }
    });
    this.myForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.maxLength(128)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(128)]),
      email: new FormControl(null, [Validators.required, Validators.email])
    });

  }


  facebookInitAndCheck() {

    (window as any).fbAsyncInit = () => {
      FB.init({
        appId: '394951114363257',
        cookie: true,
        xfbml: true,
        version: 'v3.3'
      });
      FB.AppEvents.logPageView();
    };

    ((d, s, id) => {
      let js;
      const fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');

  }

  submitLoginFacebook() {

    FB.login((response) => {

      if (response.authResponse) {
        this.dataService.loginFacebook(response.authResponse.accessToken).subscribe(result => {

          // tslint:disable-next-line: no-string-literal
          this.afterLogin(result['user'].id, result['user'].email, result['user'].role, result['token']);
        });
      } else {

      }
    }, {
      scope: 'email'
    });

  }

  register() {
    const name = this.myForm.get('name').value;
    const email = this.myForm.get('email').value;
    const password = this.myForm.get('password').value;
    this.dataService.register(name, email, password).subscribe(() => {

      this.registerBool = false;
    });
  }

  login() {
    const email = this.myForm.get('email').value;
    const password = this.myForm.get('password').value;
    this.dataService.login(email, password).subscribe(results => {
      this.afterLogin(results.data.user.id, results.data.user.email, results.data.user.role, results.data.token);
    }, error => {

      this.message = {
        text: error.status === 401 ? 'Wrong Login/Password' : `There has been an error with the backend.
         It returned the error code : ${error.status.toString()} ${error.statusText}`,
        type: 'error'
      };
      setTimeout(() => {
        this.message = {
          text: '',
          type: ''
        };
      }, 5000); // Reset the error message to empty
    });
  }

  afterLogin(id, email, role, token) {
    localStorage.setItem('userID', id);
    localStorage.setItem('userRole', role);
    localStorage.setItem('userMail', email);
    localStorage.setItem('token', JSON.stringify(token));
    this.dataService.getImage(id).subscribe(blob => {
      let profilePicture;
      const reader = new FileReader();
      const that = this;
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        profilePicture = reader.result;
        localStorage.setItem('userPicture', profilePicture);
        that.auth.updateUser({
          id,
          mail: email,
          role,
          image: profilePicture
        });
      };
    });
    this.auth.updateUser({
      id,
      mail: email,
      role,
      image: 'assets/img/Portrait_Default.png' // TODO : remove
    });
    // Set up the token refresh
    this.refresh();
    // Move to the maps page of the user that just logged in
    this.zone.run(() => {
      this.router.navigate(['/map', id]);
    });
  }

  forgottenpassword() {
    const email = this.myForm.get('email').value;
    if (this.myForm.get('email').valid) {
      this.dataService.forgottenpassword(email).subscribe(() => {

        this.message = {
          text: `An email to ${email} has been sent`,
          type: 'success'
        };
        setTimeout(() => {
          this.message = {
            text: '',
            type: ''
          };
        }, 5000); // Reset the error message to empty
      }, error => {

        this.message = {
          text: `There has been an error with the backend. It returned the error code : ${error.status.toString()} ${error.statusText}`,
          type: 'error'
        };
        setTimeout(() => {
          this.message = {
            text: '',
            type: ''
          };
        }, 5000); // Reset the error message to empty
      });
    } else {
      this.myForm.get('email').markAsTouched(); // The email field will display the invalid text
    }
  }


  refresh() {
    // Refresh the token periodically, specifically each time your token is set to expire 10% of the total time of the token
    const expireDate = new Date(JSON.parse(localStorage.getItem('token')).expiresIn).getTime();
    const timeDiff = expireDate - new Date().getTime();
    setTimeout(() => {
      const email = localStorage.getItem('userMail');
      this.dataService.refreshToken(email).subscribe(result => {


        localStorage.setItem('token', JSON.stringify(result));
        this.refresh();
      });
    }, timeDiff - timeDiff * 0.2); // End of the token minus ten percent
  }

  logout() {
    // Will ask the server to invalidate the refresh token
    this.dataService.logout(localStorage.getItem('userID')).subscribe(() => {

    });
    // We remove the informations of current user from the local storage
    localStorage.removeItem('token');
    localStorage.removeItem('userID');
    localStorage.removeItem('userMail');
    localStorage.removeItem('userPicture');
  }

}
