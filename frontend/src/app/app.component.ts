import {Component, OnInit} from '@angular/core';
import {AuthServiceService} from './auth-service.service';
import {DataServiceService} from './dataservice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'Traily';
  currentUser;
  public dataService: DataServiceService;

  constructor(public auth: AuthServiceService, dataService: DataServiceService) {
    this.dataService = dataService;
  }

  ngOnInit() {
    this.currentUser = {
      id: localStorage.getItem('userID'), mail: localStorage.getItem('userMail'),
      role: localStorage.getItem('userRole'), image: localStorage.getItem('userPicture')
    };
    this.auth.currentUserChange.subscribe(updatedUser => {
      this.currentUser = updatedUser;
    });
  }
}
