import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {AuthServiceService} from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public auth: AuthServiceService, public router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    /*
    Define if a user has the right to naviguate to a page
    */
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }
    if (route.data.role === 'admin' && !this.auth.isAdmin()) {
      this.router.navigate(['/map', this.auth.currentUser.id]);
      return false;
    } else if (route.data.role === 'coach' && !this.auth.isCoach()) {
      this.router.navigate(['/map', this.auth.currentUser.id]);
      return false;
    } else {
      return true;
    }
  }
}
