import {Routes} from '@angular/router';
import {MapComponent} from './map/map.component';
import {LoginComponent} from './login/login.component';
import {UsersettingsComponent} from './usersettings/usersettings.component';
import {AuthGuardService as AuthGuard} from './auth-guard.service';
import {UseradminComponent} from './useradmin/useradmin.component';

export const appRoutes: Routes = [{
  path: 'login',
  component: LoginComponent
},
  {
    path: 'logout',
    component: LoginComponent,
    data: {
      logout: 'logout'
    }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'map/:userid',
    component: MapComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'settings/:userid',
    component: UsersettingsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    component: UseradminComponent,
    canActivate: [AuthGuard],
    data: {role: 'admin'}
  },
  {
    path: 'coach',
    component: UseradminComponent,
    canActivate: [AuthGuard],
    data: {role: 'coach'}
  },
];
