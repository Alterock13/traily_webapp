import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MatButtonToggleModule, MatIconModule, MatPaginatorModule, MatSortModule} from '@angular/material/';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppComponent} from './app.component';
import {DialogTrackNameComponent, MapComponent} from './map/map.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login/login.component';
import {RouterModule} from '@angular/router';
import {appRoutes} from './app.routes';
import {DialogImageLoaderComponent, UsersettingsComponent} from './usersettings/usersettings.component';
import {AuthGuardService} from './auth-guard.service';
import {AuthServiceService} from './auth-service.service';
import {UseradminComponent} from './useradmin/useradmin.component';
import {RatingComponent} from './map/rating/rating.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import {ServerErrorInterceptor} from './RequestInterceptor';


@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    DialogTrackNameComponent,
    DialogImageLoaderComponent,
    LoginComponent,
    UsersettingsComponent,
    UseradminComponent,
    RatingComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes, {
        useHash: true
      }
    ),
    BrowserModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    FlexLayoutModule,
    MatDialogModule,
    MatFormFieldModule,
    MatMenuModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    HttpClientModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    ImageCropperModule,
  ],
  providers: [AuthGuardService, AuthServiceService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogTrackNameComponent, DialogImageLoaderComponent]
})
export class AppModule {
}
