import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {

  constructor(public router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /*
    Intercepts every request to redirect the user to the login page in case of a 401 status code
    */
    return next.handle(request).pipe(tap(event => {
      return event;
    }), catchError(err => {
      if (err.status === 401) {
        this.router.navigate(['login']);
      }
      return throwError(err);
    }));
  }
}
