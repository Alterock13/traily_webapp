import {browser, by, element, protractor} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getTitleText() {
    return element(by.css('app-root mat-card-title')).getText();
  }

  clickElement(selector: any, xPath?: boolean) {
    const EC = protractor.ExpectedConditions;
    if (xPath) {
      browser.wait(EC.visibilityOf(element(by.xpath(selector))), 30000);
      element(by.xpath(selector)).click();
      return;
    }
    browser.wait(EC.visibilityOf(element(by.css(selector))), 30000);
    element(by.css(selector)).click();
  }

  fillElement(selector: any, elementContent: any, xPath?: boolean) {
    const EC = protractor.ExpectedConditions;
    const elementToFill = xPath ? element(by.xpath(selector)) : element(by.css(selector));
    browser.wait(EC.visibilityOf(elementToFill), 30000);
    elementToFill.clear();
    elementToFill.sendKeys(elementContent);
  }

}
