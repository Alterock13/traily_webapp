Feature: Trainee
  Assign trainee

  Scenario: Unassign followed trainee
    Given I am on the home page
    When I login as coach1
    When I visit the coach panel
    When I stop coaching trainee1
    Then I should not have any trainee

  Scenario: Follow trainee
    Given I am on the home page
    When I login as coach1
    When I visit the coach panel
    When I follow trainee1
    Then I should have 1 trainee
