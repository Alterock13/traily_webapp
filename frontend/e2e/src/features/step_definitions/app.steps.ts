import {Before, Given, Then, When} from 'cucumber';
import {AppPage} from '../../app.po';

const expect = require('expect');

const {setDefaultTimeout} = require('cucumber');
setDefaultTimeout(60 * 1000);
let page: AppPage;

Before(() => {
  page = new AppPage();
});


Given(/^I am on the home page$/, async () => {
  await page.navigateTo();
});

When(/^I do nothing$/, () => {
});

Then(/^I should see the title$/, async () => {
  expect(await page.getTitleText()).toEqual('Welcome');
});

