import {Before, Then, When} from 'cucumber';
import {AppPage} from '../../app.po';
import {browser, by, element} from 'protractor';


const expect = require('expect');

const {setDefaultTimeout} = require('cucumber');
setDefaultTimeout(60 * 1000);

let page: AppPage;

Before(() => {
  page = new AppPage();
});


When(/^I login as trainee1$/, async () => {
  await browser.waitForAngularEnabled(false);
  page.fillElement('//input[@formcontrolname="email"]', 'trainee1@traily.club', true);
  page.fillElement('//input[@formcontrolname="password"]', '1trainee1', true);
  page.clickElement('.left');
});

Then(/^I should see the map$/, async () => {
  expect(element(by.css('.map')).isPresent());
});
