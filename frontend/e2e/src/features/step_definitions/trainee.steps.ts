import {AppPage} from '../../app.po';
import {browser, by, element, protractor} from 'protractor';
import {Before, Then, When} from 'cucumber';

const expect = require('expect');


let page: AppPage;

const {setDefaultTimeout} = require('cucumber');
setDefaultTimeout(60 * 1000);

Before(() => {
  page = new AppPage();
});

When(/^I login as coach1$/, async () => {
  await browser.waitForAngularEnabled(false);
  page.fillElement('//*[@formcontrolname="email"]', 'coach1@traily.club', true);
  page.fillElement('//*[@formcontrolname="password"]', '1coach1', true);
  page.clickElement('.left');
});

When(/^I visit the coach panel$/, () => {
  page.clickElement('/html/body/app-root/mat-toolbar/img[2]', true);
  page.clickElement('//*[@id="cdk-overlay-0"]/div/div/button[3]', true);
});

When(/^I stop coaching trainee1$/, () => {
  page.clickElement('/html/body/app-root/mat-drawer-container/mat-drawer-content/app-useradmin/div/' +
    'mat-card[1]/mat-card-content/div/mat-table/mat-row/mat-cell[4]/mat-icon', true);
});

Then(/^I should not have any trainee$/, () => {
  browser.driver.findElements(by.css('app-root mat-row')).then(elems => {
      expect(elems.length).toEqual(0);
    }
  );
});

When(/^I follow trainee1$/, () => {
  page.fillElement('//input[@placeholder="Email"]', 'trainee1@traily.club', true);
  page.clickElement('//button/span[text()="Confirm"]/..', true);
});

Then(/^I should have (\d+) trainee$/, (numberOfTrainees: number) => {
  const EC = protractor.ExpectedConditions;
  browser.wait(EC.visibilityOf(element(by.css('app-root mat-row'))), 30000);
  browser.driver.findElements(by.css('app-root mat-row')).then(elems => {
      console.log('######' + elems.length);
      console.log('######' + numberOfTrainees + '######');
      expect(elems.length).toBeGreaterThan(0);
    }
  );
});
