# Quick Start

## G�n�ration de certificat

Le lancement de l'application n�cessite l'utilisation de certificat SSL. Pour ce faire, il faut g�n�rer un certificat au format CRT et une cl� KEY � l'aide de la commande:

```
openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

Deux fichiers localhost.crt et localhost.key seront cr��s dans le dossier dans lequel vous �tes. Ces deux fichiers doivent �tre plac�s dans les deux chemins backend/src/config/https/ et frontend/src/assets/https/.

## Backend

Apr�s avoir g�n�r� et d�plac� les certificats cr��s dans l'�tape pr�c�dente, il faut cr�er un fichier d'environnement .env � partir de l'exemple fourni .env.example.

Les deux champs � renseigner sont:

```
MONGO_URI pour pointer le Backend vers l'instance et base de donn�e MongoDB que vous avez cr�� (en local ou sur un serveur)
MONGO_URI_TESTS pour pointer le Backend vers l'instance et base de donn�e qui sera utilis�e pour faire tourner les tests.
```

le Backend peut maintenant �tre lanc�:

Pour le lancer dans un environnement de d�veloppement avec une base de donn�es MongoDB pr�remplie :

Soit dans un environnement de d�veloppement avec une base de donn�es MongoDB pr�remplie avec les commandes suivantes:

```
cd backend
yarn install
yarn dev
```

Soit dans un environnement de production avec une base de donn�es vide avec les commandes suivantes:

```
cd backend
yarn install
yarn start
```

Une documentation de l'API backend peut �tre g�n�r�e et consult�e � l'aide des commandes:

```
cd backend
yarn install
yarn docs
```

## Frontend

Le Frontend peut �tre maintenant lanc� apr�s avoir g�n�r� les certificats:

Soit dans un environnement utilisant les deux certificats g�n�r�s � l'aide des commandes:

```
cd frontend
yarn install
yarn ssl
```

Soit dans un environnement dans lequel le d�veloppeur utilisera les certificats Apache ou NGINX utilis�s dans leurs configurations:

```
cd frontend
yarn install
yarn start
```

L'application est maintenant accessible sur https://localhost:4200 