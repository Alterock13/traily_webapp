export {};
const request = require('supertest');
const httpStatus = require('http-status');
const {expect} = require('chai');
const sinon = require('sinon');
const bcrypt = require('bcryptjs');
const {omitBy, isNil} = require('lodash');
const app = require('../../src/index');
import {User} from '../../src/api/models';

const JWT_EXPIRATION_MINUTES = require('config/vars').JWT_EXPIRATION_MINUTES;

/**
 * root level hooks
 */

async function format(user: any) {
    const formated = user;

    // delete password
    delete formated.password;

    // get users from database
    const dbUser = (await User.findOne({email: user.email})).transform();

    // remove null and undefined properties
    return omitBy(dbUser, isNil);
}

describe('Users API', async () => {
    let adminAccessToken: any;
    let coach1AccessToken: any;
    let coach2AccessToken: any;
    let trainee1AccessToken: any;
    let trainee2AccessToken: any;
    let dbUsers: any;
    let noDBTrainee: any;
    let noDBAdmin: any;

    const password = '123456';
    const passwordHashed = await bcrypt.hash(password, 1);


    beforeEach(async () => {
        await User.deleteMany({});

        dbUsers = {
            admin: {
                email: 'admin1@traily.club',
                password: passwordHashed,
                name: 'Admin One',
                role: 'admin'
            },
            trainee1: {
                email: 'trainee1@traily.club',
                password: passwordHashed,
                name: 'Trainee One',
                role: 'trainee',
                coach: null
            },
            trainee2: {
                email: 'trainee2@traily.club',
                password: passwordHashed,
                name: 'Trainee Two',
                role: 'trainee',
                coach: null
            },
            coach1: {
                email: 'coach1@traily.club',
                password: passwordHashed,
                name: 'Coach One',
                role: 'coach'
            },
            coach2: {
                email: 'coach2@traily.club',
                password: passwordHashed,
                name: 'Coach Two',
                role: 'coach'
            }
        };

        noDBTrainee = {
            email: 'trainee1nodb@traily.club',
            password,
            name: 'Trainee One'
        };

        noDBAdmin = {
            email: 'admin.nodb@traily.club',
            password,
            name: 'Admin One',
            role: 'admin'
        };

        await User.insertMany([dbUsers.admin, dbUsers.coach1, dbUsers.coach2]);
        dbUsers.trainee1.coach = await User.findOne({'email': dbUsers.coach1.email});
        dbUsers.trainee2.coach = await User.findOne({'email': dbUsers.coach2.email});
        await User.insertMany([dbUsers.trainee1, dbUsers.trainee2]);
        dbUsers.admin.password = password;
        dbUsers.trainee1.password = password;
        dbUsers.trainee2.password = password;
        dbUsers.coach1.password = password;
        dbUsers.coach2.password = password;
        adminAccessToken = (await User.findAndGenerateToken(dbUsers.admin)).accessToken;
        trainee1AccessToken = (await User.findAndGenerateToken(dbUsers.trainee1)).accessToken;
        trainee2AccessToken = (await User.findAndGenerateToken(dbUsers.trainee2)).accessToken;
        coach1AccessToken = (await User.findAndGenerateToken(dbUsers.coach1)).accessToken;
        coach2AccessToken = (await User.findAndGenerateToken(dbUsers.coach2)).accessToken;
    });

    describe('POST /api/users', () => {
        it('should create a new user when request is ok', () => {
            return request(app)
                .post('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBAdmin)
                .expect(httpStatus.CREATED)
                .then((res: any) => {
                    delete noDBAdmin.password;
                    expect(res.body).to.include(noDBAdmin);
                });
        });

        it('should create a new user and set default role to "trainee"', () => {
            return request(app)
                .post('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBTrainee)
                .expect(httpStatus.CREATED)
                .then((res: any) => {
                    expect(res.body.role).to.be.equal('trainee');
                });
        });

        it('should report error when email already exists', () => {
            noDBTrainee.email = dbUsers.trainee1.email;

            return request(app)
                .post('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBTrainee)
                .expect(httpStatus.CONFLICT)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field).to.be.equal('email');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"email" already exists');
                });
        });

        it('should report error when email is not provided', () => {
            delete noDBTrainee.email;

            return request(app)
                .post('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBTrainee)
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('email');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"email" is required');
                });
        });

        it('should report error when password length is less than 6', () => {
            noDBTrainee.password = '12345';

            return request(app)
                .post('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBTrainee)
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('password');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"password" length must be at least 6 characters long');
                });
        });

        it('should report error when logged user is not an admin', () => {
            return request(app)
                .post('/api/users')
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send(noDBTrainee)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Unknown role');
                });
        });
    });

    describe('GET /api/users', () => {
        it('should get all users', () => {
            return request(app)
                .get('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.OK)
                .then(async (res: any) => {
                    const {data} = res.body;
                    expect(data).to.be.an('array');
                    expect(data).to.have.lengthOf(5);
                });
        });

        it('should get all users with pagination', () => {
            return request(app)
                .get('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .query({page: 2, perPage: 1})
                .expect(httpStatus.OK)
                .then(async (res: any) => {
                    delete dbUsers.trainee1.password;
                    const {data} = res.body;

                    expect(data).to.be.an('array');
                    expect(data).to.have.lengthOf(1);
                });
        });

        it('should filter users', () => {
            return request(app)
                .get('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .query({email: dbUsers.trainee1.email})
                .expect(httpStatus.OK)
                .then(async (res: any) => {
                    delete dbUsers.trainee1.password;

                    const {data} = res.body;

                    expect(data).to.be.an('array');
                    expect(data).to.have.lengthOf(1);
                });
        });

        it('should report error when pagination\'s parameters are not a number', () => {
            return request(app)
                .get('/api/users')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .query({page: '?', perPage: 'whaat'})
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('page');
                    expect(location).to.be.equal('query');
                    expect(messages).to.include('"page" must be a number');
                    return Promise.resolve(res);
                })
                .then((res: any) => {
                    const {field} = res.body.errors[1];
                    const {location} = res.body.errors[1];
                    const {messages} = res.body.errors[1];
                    expect(field[0]).to.be.equal('perPage');
                    expect(location).to.be.equal('query');
                    expect(messages).to.include('"perPage" must be a number');
                });
        });

        it('should report error if logged user is not an admin', () => {
            return request(app)
                .get('/api/users')
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Unknown role');
                });
        });
    });

    describe('GET /api/users/:userId', () => {
        it('should get user', async () => {
            const id = (await User.findOne({}))._id;
            delete dbUsers.admin.password;

            return request(app)
                .get(`/api/users/${id}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body).to.include(dbUsers.admin);
                });
        });

        it('should report error "User does not exist" when user does not exists', () => {
            return request(app)
                .get('/api/users/56c787ccc67fc16ccc1a5e92')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(404);
                    expect(res.body.message).to.be.equal('User does not exist');
                });
        });

        it('should report error "User does not exist" when id is not a valid ObjectID', () => {
            return request(app)
                .get('/api/users/random')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(404);
                    expect(res.body.message).to.equal('User does not exist');
                });
        });

        it('should report error when logged user is not the same as the requested one', async () => {
            const id = (await User.findOne({email: dbUsers.admin.email}))._id;

            return request(app)
                .get(`/api/users/${id}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });
    });

    describe('PUT /api/users/:userId', () => {
        it('should replace user', async () => {
            delete dbUsers.admin.password;
            const id = (await User.findOne(dbUsers.admin))._id;

            return request(app)
                .put(`/api/users/${id}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBTrainee)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    delete noDBTrainee.password;
                    expect(res.body).to.include(noDBTrainee);
                    expect(res.body.role).to.be.equal('trainee');
                });
        });

        it('should report error when email is not provided', async () => {
            const id = (await User.findOne({}))._id;
            delete noDBTrainee.email;

            return request(app)
                .put(`/api/users/${id}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBTrainee)
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('email');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"email" is required');
                });
        });

        it('should report error user when password length is less than 6', async () => {
            const id = (await User.findOne({}))._id;
            noDBTrainee.password = '12345';

            return request(app)
                .put(`/api/users/${id}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBTrainee)
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('password');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"password" length must be at least 6 characters long');
                });
        });

        it('should report error "User does not exist" when user does not exists', () => {
            return request(app)
                .put('/api/users/random')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(404);
                    expect(res.body.message).to.be.equal('User does not exist');
                });
        });

        it('should report error when logged user is not the same as the requested one', async () => {
            const id = (await User.findOne({email: dbUsers.admin.email}))._id;

            return request(app)
                .put(`/api/users/${id}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should not replace the role of the user (not admin)', async () => {
            const id = (await User.findOne({email: dbUsers.trainee1.email}))._id;
            const role = 'admin';

            return request(app)
                .put(`/api/users/${id}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send(noDBAdmin)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.role).to.not.be.equal(role);
                });
        });
    });

    describe('PATCH /api/users/:userId', () => {
        it('should update user', async () => {
            delete dbUsers.admin.password;
            const id = (await User.findOne(dbUsers.admin))._id;
            const {name} = noDBTrainee;

            return request(app)
                .patch(`/api/users/${id}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send({name})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.name).to.be.equal(name);
                    expect(res.body.email).to.be.equal(dbUsers.admin.email);
                });
        });

        it('should not update user when no parameters were given', async () => {
            delete dbUsers.admin.password;
            const id = (await User.findOne(dbUsers.admin))._id;

            return request(app)
                .patch(`/api/users/${id}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send()
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body).to.include(dbUsers.admin);
                });
        });

        it('should report error "User does not exist" when user does not exists', () => {
            return request(app)
                .patch('/api/users/random')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(404);
                    expect(res.body.message).to.be.equal('User does not exist');
                });
        });

        it('should report error when logged user is not the same as the requested one', async () => {
            const id = (await User.findOne({email: dbUsers.admin.email}))._id;

            return request(app)
                .patch(`/api/users/${id}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should not update the role of the user (not admin)', async () => {
            const id = (await User.findOne({email: dbUsers.trainee1.email}))._id;
            const role = 'admin';

            return request(app)
                .patch(`/api/users/${id}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send({role})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.role).to.not.be.equal(role);
                });
        });
    });

    describe('DELETE /api/users/:userId', () => {
        it('should delete user as admin', async () => {
            const id = (await User.findOne({'email': dbUsers.trainee1.email}))._id;
            return request(app)
                .delete(`/api/users/${id}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.NO_CONTENT)
                .then(() => request(app).get('/api/users'))
                .then(async () => {
                    const users = await User.find({});
                    expect(users).to.have.lengthOf(4);
                });
        });

        it('should delete user as requested user', async () => {
            const id = (await User.findOne({'email': dbUsers.trainee1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.NO_CONTENT)
                .then(() => request(app).get('/api/users'))
                .then(async () => {
                    const users = await User.find({});
                    expect(users).to.have.lengthOf(4);
                });
        });

        it('should delete user as user\'s coach', async () => {
            const id = (await User.findOne({'email': dbUsers.trainee1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .expect(httpStatus.NO_CONTENT)
                .then(() => request(app).get('/api/users'))
                .then(async () => {
                    const users = await User.find({});
                    expect(users).to.have.lengthOf(4);
                });
        });

        it('should report error when logged user is not the same as the requested one', async () => {
            const id = (await User.findOne({email: dbUsers.trainee1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}`)
                .set('Authorization', `Bearer ${trainee2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should report error when logged user is not the user\'s coach', async () => {
            const id = (await User.findOne({email: dbUsers.coach2.email}))._id;

            return request(app)
                .delete(`/api/users/${id}`)
                .set('Authorization', `Bearer ${trainee2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });


        it('should report error "User does not exist" when user does not exists', () => {
            return request(app)
                .delete('/api/users/random')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(404);
                    expect(res.body.message).to.be.equal('User does not exist');
                });
        });


    });

    describe('GET /api/users/profile', () => {
        it('should get the logged user\'s info', () => {
            delete dbUsers.trainee1.password;
            delete dbUsers.trainee1.coach;

            return request(app)
                .get('/api/users/profile')
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body).to.include(dbUsers.trainee1);
                });
        });

        it('should report error without stacktrace when accessToken is expired', async () => {
            // fake time
            const clock = sinon.useFakeTimers();
            const expiredAccessToken = (await User.findAndGenerateToken(dbUsers.admin)).accessToken;

            // move clock forward by minutes set in config + 1 minute
            clock.tick(JWT_EXPIRATION_MINUTES * 60000 + 60000);

            return request(app)
                .get('/api/users/profile')
                .set('Authorization', `Bearer ${expiredAccessToken}`)
                .expect(httpStatus.UNAUTHORIZED)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.UNAUTHORIZED);
                    expect(res.body.message).to.be.equal('jwt expired');
                    expect(res.body).to.not.have.a.property('stack');
                });
        });
    });
});
