export {};
const request = require('supertest');
const httpStatus = require('http-status');
const mongoose = require('mongoose');
const {expect} = require('chai');
const bcrypt = require('bcryptjs');
const app = require('../../src/index');
const ObjectId = mongoose.Types.ObjectId;
import {User, UserTrack} from '../../src/api/models';
import {computeDistance, generateRandomDate, generateRandomPoints, getRandomInt} from '../../src/api/utils/Utils';


describe('Tracks API', async () => {
    let adminAccessToken: any;
    let coach1AccessToken: any;
    let coach2AccessToken: any;
    let trainee1AccessToken: any;
    let trainee2AccessToken: any;
    let traineeUser1: any;
    let traineeUser2: any;
    let coachUser1: any;
    let coachUser2: any;
    let adminUser1: any;
    let track0User1: any;
    let noDBTrack: any;
    let TRACK_USER_1: any;

    beforeEach(async () => {
        await User.deleteMany({});
        await UserTrack.deleteMany({});

        const password = '123456';
        // const passwordHashed = password;
        const passwordHashed = await bcrypt.hash(password, 1);

        const ADMIN_1 = {
            email: 'admin@traily.club',
            role: 'admin',
            password: passwordHashed,
            name: 'admin one'
        };

        const COACH_1 = {
            email: 'coach1@traily.club',
            role: 'coach',
            password: passwordHashed,
            name: 'coach one'
        };

        const COACH_2 = {
            email: 'coach2@traily.club',
            role: 'coach',
            password: passwordHashed,
            name: 'coach two'
        };


        await User.insertMany([ADMIN_1, COACH_1, COACH_2]);

        adminUser1 = await User.findOne(ADMIN_1);
        coachUser1 = await User.findOne(COACH_1);
        coachUser2 = await User.findOne(COACH_2);

        const TRAINEE_1 = {
            email: 'trainee1@traily.club',
            role: 'trainee',
            password: passwordHashed,
            name: 'trainee one',
            coach: coachUser1.id
        };

        const TRAINEE_2 = {
            email: 'trainee2@traily.club',
            role: 'trainee',
            password: passwordHashed,
            name: 'trainee two',
            coach: coachUser2.id
        };

        await User.insertMany([TRAINEE_1, TRAINEE_2]);
        traineeUser1 = await User.findOne({email: TRAINEE_1.email});
        traineeUser2 = await User.findOne({email: TRAINEE_2.email});


        // await UserTrack.insertMany(tracks);

        TRACK_USER_1 = {
            user: traineeUser1,
            name: `trainee1 track 0`,
            rating: getRandomInt(6),
            locations: generateRandomPoints({'lat': 45.1841602, 'lng': 5.6805232}, 1000, 10)
        };

        // Add trainees tracks
        await UserTrack.insertMany([TRACK_USER_1]);
        track0User1 = await UserTrack.findOne({user: TRACK_USER_1.user, name: TRACK_USER_1.name});
        let lat = 45.1841602;
        let lng = 5.6805232;
        for (let i = 0; i < 10; i += 1) {
            lat += i * 0.02;
            lng += i * 0.02;
            const geoLocs = generateRandomPoints({'lat': lat, 'lng': lng}, 1000, 10);
            const geoDistance = computeDistance(geoLocs);
            const track = {
                user: traineeUser1,
                name: `trainee1 track ${i + 1}`,
                rating: getRandomInt(6),
                locations: geoLocs,
                distance: geoDistance,
                createdAt: generateRandomDate(new Date(2019, 3, 1), new Date())
            };
            const track2 = {
                user: traineeUser2,
                name: `trainee2 track ${i + 1}`,
                rating: getRandomInt(6),
                locations: geoLocs,
                distance: geoDistance,
                createdAt: generateRandomDate(new Date(2019, 3, 1), new Date())
            };
            await UserTrack.insertMany([track, track2]);
        }

        noDBTrack = {
            name: 'trainee1 inserted track',
            duration: 4800000,
            locations: generateRandomPoints({'lat': 45.1841602, 'lng': 5.6805232}, 1000, 10)
        };

        ADMIN_1.password = password;
        COACH_1.password = password;
        COACH_2.password = password;
        TRAINEE_1.password = password;
        TRAINEE_2.password = password;

        adminAccessToken = (await User.findAndGenerateToken(ADMIN_1)).accessToken;
        coach1AccessToken = (await User.findAndGenerateToken(COACH_1)).accessToken;
        coach2AccessToken = (await User.findAndGenerateToken(COACH_2)).accessToken;
        trainee1AccessToken = (await User.findAndGenerateToken(TRAINEE_1)).accessToken;
        trainee2AccessToken = (await User.findAndGenerateToken(TRAINEE_2)).accessToken;
    });


    describe('GET /api/users/:userId/tracks', () => {
        it('should return the user\'s tracks', async () => {
            const userId = traineeUser1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    res.body.data.forEach((track: any) => {
                        expect(track.user).to.be.equal(userId);
                    });
                });
        });

        it('should report error when user does not exist', async () => {
            return request(app)
                .get(`/api/users/random/tracks`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.NOT_FOUND);
                    expect(res.body.message).to.be.equal('User not found');
                });
        });

        it('should report error when logged user is not the tracks owner', async () => {
            const userId = traineeUser1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${trainee2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should return the coach trainee\'s tracks', async () => {
            const userId = traineeUser1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    res.body.data.forEach((track: any) => {
                        expect(track.user).to.be.equal(userId);
                    });
                });
        });

        it('should report error when logged user is not the trainee\'s coach', async () => {
            const userId = traineeUser1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${coach2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should return the trainee\'s tracks when logged user is an admin', async () => {
            const userId = traineeUser1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    res.body.data.forEach((track: any) => {
                        expect(track.user).to.be.equal(userId);
                    });
                });
        });
    });

    describe('POST /api/users/:userId/tracks', () => {
        it('should create a new track as user', () => {
            const userId = traineeUser1.id;
            return request(app)
                .post(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send(noDBTrack)
                .expect(httpStatus.CREATED)
                .then((res: any) => {
                    expect(res.body.user).to.be.equal(userId);
                    expect(res.body.name).to.be.equal(noDBTrack.name);
                    expect(res.body.duration).to.be.equal(noDBTrack.duration);
                    expect(res.body.rating).to.be.equal(0);
                    expect(res.body.locations[0]).to.have.lengthOf(2);
                    expect(res.body.locations).to.have.lengthOf(10);
                });
        });

        it('should report an error when logged user is a coach', () => {
            const userId = traineeUser1.id;
            return request(app)
                .post(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .send(noDBTrack)
                .expect(httpStatus.FORBIDDEN);
        });

        it('should create a new track as admin', () => {
            const userId = traineeUser1.id;
            return request(app)
                .post(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send(noDBTrack)
                .expect(httpStatus.CREATED)
                .then((res: any) => {
                    expect(res.body.user).to.be.equal(userId);
                    expect(res.body.name).to.be.equal(noDBTrack.name);
                    expect(res.body.duration).to.be.equal(noDBTrack.duration);
                    expect(res.body.rating).to.be.equal(0);
                    expect(res.body.locations[0]).to.have.lengthOf(2);
                    expect(res.body.locations).to.have.lengthOf(10);
                });
        });

        it('should report an error when logged user is not same as the requested', () => {
            const userId = traineeUser1.id;
            return request(app)
                .post(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${trainee2AccessToken}`)
                .send(noDBTrack)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should report an error when name is missing', () => {
            const userId = traineeUser1.id;
            delete noDBTrack.name;
            return request(app)
                .post(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send(noDBTrack)
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.BAD_REQUEST);
                });
        });

        it('should report an error when locations are missing', () => {
            const userId = traineeUser1.id;
            delete noDBTrack.locations;
            return request(app)
                .post(`/api/users/${userId}/tracks`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send(noDBTrack)
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.BAD_REQUEST);
                });
        });
    });

    describe('PATCH /api/users/:userId/tracks/:trackId', () => {
        it('should update the track\'s name as user', async () => {
            const trackName = "suicide run 42";
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .patch(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send({name: trackName})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.name).to.be.equal(trackName);
                    expect(res.body.name).to.not.be.equal(track0User1.name);
                    expect(res.body.duration).to.be.equal(track0User1.duration);
                });
        });

        it('should update the track\'s rating as coach', async () => {
            const trackRating = 4;
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .patch(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .send({rating: trackRating})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.rating).to.be.equal(trackRating);
                    expect(res.body.name).to.be.equal(track0User1.name);
                });
        });

        it('should report an error when user tries to update a track\'s rating', async () => {
            const trackRating = 4;
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .patch(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send({rating: trackRating})
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.status).to.be.equal(httpStatus.FORBIDDEN);
                });
        });

        it('should report an error when coach tries to update a track\'s name', async () => {
            const trackName = "suicide run 42";
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .patch(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .send({name: trackName})
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.status).to.be.equal(httpStatus.FORBIDDEN);
                });
        });


        it('should update the track\'s name and rating as admin', async () => {
            const trackRating = 4;
            const trackName = "suicide run 42";
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .patch(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send({name: trackName, rating: trackRating})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.rating).to.be.equal(trackRating);
                    expect(res.body.name).to.be.equal(trackName);
                });
        });
    });

    describe('GET /api/users/:userId/tracks/:trackId', () => {
        it('should return the track', async () => {
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.name).to.be.equal(track0User1.name);
                    expect(res.body.user).to.be.equal(userId);
                });
        });

        it('should return the track when logged user is admin', async () => {
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.name).to.be.equal(track0User1.name);
                    expect(res.body.user).to.be.equal(userId);
                });
        });

        it('should return the track when logged user is user\'s coach', async () => {
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.name).to.be.equal(track0User1.name);
                    expect(res.body.user).to.be.equal(userId);
                });
        });

        it('should report error when track owner does not exist', async () => {
            const trackId = track0User1.id;
            return request(app)
                .get(`/api/users/random/tracks/${trackId}`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.NOT_FOUND);
                    expect(res.body.message).to.be.equal('User not found');
                });
        });

        it('should report error when track does not exist', async () => {
            const userId = traineeUser1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks/random`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.NOT_FOUND);
                    expect(res.body.message).to.be.equal('Track does not exist');
                });
        });

        it('should report error when logged user is not owner', async () => {
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${trainee2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should report error when logged user is not the trainee\'s coach', async () => {
            const userId = traineeUser1.id;
            const trackId = track0User1.id;
            return request(app)
                .get(`/api/users/${userId}/tracks/${trackId}`)
                .set('Authorization', `Bearer ${coach2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });
    });

    describe('DELETE /api/users/:userId/tracks', () => {
        it('should delete user\'s tracks', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}/tracks`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.NO_CONTENT)
                .then(async () => {
                    const userTracks = await UserTrack.find({'user': new ObjectId(id)});
                    expect(userTracks).to.have.lengthOf(0);
                });
        });

        it('should delete user\'s tracks as coach', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}/tracks`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .expect(httpStatus.NO_CONTENT)
                .then(async () => {
                    const userTracks = await UserTrack.find({'user': new ObjectId(id)});
                    expect(userTracks).to.have.lengthOf(0);
                });
        });

        it('should delete user\'s tracks as admin', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}/tracks`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.NO_CONTENT)
                .then(async () => {
                    let userTracks = await UserTrack.find({'user': new ObjectId(id)});
                    expect(userTracks).to.have.lengthOf(0);
                    userTracks = await UserTrack.find({'user': traineeUser2._id});
                    expect(userTracks).to.have.lengthOf(10);
                });
        });

        it('should report an error when logged user is not the trainee', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}/tracks`)
                .set('Authorization', `Bearer ${trainee2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should report an error when logged user is not the trainee\'s coach', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}/tracks`)
                .set('Authorization', `Bearer ${coach2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });


        it('should report an error when logged user is not the trainee\'s coach', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;

            return request(app)
                .delete(`/api/users/${id}/tracks`)
                .set('Authorization', `Bearer ${coach2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should report error when track owner does not exist', async () => {
            return request(app)
                .delete('/api/users/random/tracks/')
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.NOT_FOUND)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.NOT_FOUND);
                    expect(res.body.message).to.be.equal('User not found');
                });
        });
    });
});
