export {};
const request = require('supertest');
const httpStatus = require('http-status');
const {expect} = require('chai');
const sinon = require('sinon');
const moment = require('moment-timezone');
const app = require('../../src/index');
import {User} from '../../src/api/models';

const RefreshToken = require('../../src/api/models/refreshToken.model');
const authProviders = require('../../src/api/services/authProviders');

const sandbox = sinon.createSandbox();

const fakeOAuthRequest = () =>
    Promise.resolve({
        service: 'facebook',
        id: '123',
        name: 'user',
        email: 'test@test.com'
    });

describe('Authentication API', () => {
    let dbUser: any;
    let logoutUser: any;
    let user: any;
    let refreshToken: any;
    let expiredRefreshToken: any;

    beforeEach(async () => {
        dbUser = {
            email: 'branstark@gmail.com',
            password: 'mypassword',
            name: 'Bran Stark',
            role: 'admin'
        };

        logoutUser = {
            email: 'logmeout@gmail.com',
            password: 'mypassword',
            name: 'Log Me Out',
            role: 'trainee'
        };

        user = {
            email: 'trainee1@gmail.com',
            password: '123456',
            name: 'Trainee One'
        };

        refreshToken = {
            token:
                '5947397b323ae82d8c3a333b.c69d0435e62c9f4953af912442a3d064e20291f0d228c0552ed4be473e7d191ba40b18c2c47e8b9d',
            userId: '5947397b323ae82d8c3a333b',
            userEmail: dbUser.email,
            expires: moment()
                .add(1, 'day')
                .toDate()
        };


        expiredRefreshToken = {
            token:
                '5947397b323ae82d8c3a333b.c69d0435e62c9f4953af912442a3d064e20291f0d228c0552ed4be473e7d191ba40b18c2c47e8b9d',
            userId: '5947397b323ae82d8c3a333b',
            userEmail: dbUser.email,
            expires: moment()
                .subtract(1, 'day')
                .toDate()
        };


        await User.deleteMany({});
        await User.create(dbUser);
        await User.create(logoutUser);
        await RefreshToken.deleteMany({});
    });

    afterEach(() => sandbox.restore());

    describe('POST /api/auth/register', () => {
        it('should register a new user when request is ok', () => {
            return request(app)
                .post('/api/auth/register')
                .send(user)
                .expect(httpStatus.CREATED)
                .then((res: any) => {
                    delete user.password;
                    expect(res.body.data.token).to.have.a.property('accessToken');
                    expect(res.body.data.token).to.have.a.property('refreshToken');
                    expect(res.body.data.token).to.have.a.property('expiresIn');
                    expect(res.body.data.user).to.include(user);
                });
        });

        it('should report error when email already exists', () => {
            return request(app)
                .post('/api/auth/register')
                .send(dbUser)
                .expect(httpStatus.CONFLICT)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field).to.be.equal('email');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"email" already exists');
                });
        });

        it('should report error when the email provided is not valid', () => {
            user.email = 'this_is_not_an_email';
            return request(app)
                .post('/api/auth/register')
                .send(user)
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('email');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"email" must be a valid email');
                });
        });

        it('should report error when email and password are not provided', () => {
            return request(app)
                .post('/api/auth/register')
                .send({})
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('email');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"email" is required');
                });
        });
    });

    describe('POST /api/auth/login', () => {
        it('should return an accessToken and a refreshToken when email and password matches', () => {
            return request(app)
                .post('/api/auth/login')
                .send(dbUser)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    delete dbUser.password;
                    expect(res.body.data.token).to.have.a.property('accessToken');
                    expect(res.body.data.token).to.have.a.property('refreshToken');
                    expect(res.body.data.token).to.have.a.property('expiresIn');
                    expect(res.body.data.user).to.include(dbUser);
                });
        });

        it('should report error when email and password are not provided', () => {
            return request(app)
                .post('/api/auth/login')
                .send({})
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('email');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"email" is required');
                });
        });

        it('should report error when the email provided is not valid', () => {
            user.email = 'this_is_not_an_email';
            return request(app)
                .post('/api/auth/login')
                .send(user)
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const {field} = res.body.errors[0];
                    const {location} = res.body.errors[0];
                    const {messages} = res.body.errors[0];
                    expect(field[0]).to.be.equal('email');
                    expect(location).to.be.equal('body');
                    expect(messages).to.include('"email" must be a valid email');
                });
        });

        it('should report error when email and password don\'t match', () => {
            dbUser.password = 'xxx';
            return request(app)
                .post('/api/auth/login')
                .send(dbUser)
                .expect(httpStatus.UNAUTHORIZED)
                .then((res: any) => {
                    const {code} = res.body;
                    const {message} = res.body;
                    expect(code).to.be.equal(401);
                    expect(message).to.be.equal('Incorrect email or password');
                });
        });
    });

    describe('POST /api/auth/refresh-token', () => {
        it('should return a new accessToken when refreshToken and email match', async () => {
            await RefreshToken.create(refreshToken);
            return request(app)
                .post('/api/auth/refresh-token')
                .send({email: dbUser.email, refreshToken: refreshToken.token})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body).to.have.a.property('accessToken');
                    expect(res.body).to.have.a.property('refreshToken');
                    expect(res.body).to.have.a.property('expiresIn');
                });
        });

        it('should report error when email and refreshToken don\'t match', async () => {
            await RefreshToken.create(refreshToken);
            return request(app)
                .post('/api/auth/refresh-token')
                .send({email: user.email, refreshToken: refreshToken.token})
                .expect(httpStatus.UNAUTHORIZED)
                .then((res: any) => {
                    const {code} = res.body;
                    const {message} = res.body;
                    expect(code).to.be.equal(401);
                    expect(message).to.be.equal('Incorrect email or refreshToken');
                });
        });

        it('should report error when email and refreshToken are not provided', () => {
            return request(app)
                .post('/api/auth/refresh-token')
                .send({})
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    const field1 = res.body.errors[0].field;
                    const location1 = res.body.errors[0].location;
                    const messages1 = res.body.errors[0].messages;
                    const field2 = res.body.errors[1].field;
                    const location2 = res.body.errors[1].location;
                    const messages2 = res.body.errors[1].messages;
                    expect(field1[0]).to.be.equal('email');
                    expect(location1).to.be.equal('body');
                    expect(messages1).to.include('"email" is required');
                    expect(field2[0]).to.be.equal('refreshToken');
                    expect(location2).to.be.equal('body');
                    expect(messages2).to.include('"refreshToken" is required');
                });
        });

        it('should report error when the refreshToken is expired', async () => {
            await RefreshToken.create(expiredRefreshToken);

            return request(app)
                .post('/api/auth/refresh-token')
                .send({email: dbUser.email, refreshToken: expiredRefreshToken.token})
                .expect(httpStatus.UNAUTHORIZED)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(401);
                    expect(res.body.message).to.be.equal('Invalid refresh token.');
                });
        });
    });


    describe('POST /api/auth/logout', () => {
        it('should report error when the userId is missing', async () => {
            return request(app)
                .post('/api/auth/logout')
                .send({})
                .expect(httpStatus.BAD_REQUEST)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.BAD_REQUEST);
                });
        });

        it('should delete all refresh tokens of user', async () => {
            let userId: String;
            return request(app)
                .post('/api/auth/login')
                .send(logoutUser)
                .then(async (res: any) => {
                    userId = res.body.data.user.id;
                    const refreshTokenCount = await RefreshToken.count({'userId': userId});
                    expect(refreshTokenCount).to.not.be.equal(0);
                })
                .then(() => request(app).post('/api/auth/logout').send({'userId': userId}))
                .then(async () => {
                    const refreshTokenCount = await RefreshToken.count({'userId': userId});
                    expect(refreshTokenCount).to.be.equal(0);
                });
        });
    });
});
