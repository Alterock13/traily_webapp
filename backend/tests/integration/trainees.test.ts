export {};
const request = require('supertest');
const httpStatus = require('http-status');
const mongoose = require('mongoose');
const {expect} = require('chai');
const bcrypt = require('bcryptjs');
const app = require('../../src/index');
const ObjectId = mongoose.Types.ObjectId;
import {User, UserTrack} from '../../src/api/models';
import {generateRandomPoints, getRandomInt} from '../../src/api/utils/Utils';


describe('Trainees API', async () => {
    let adminAccessToken: any;
    let coach1AccessToken: any;
    let coach2AccessToken: any;
    let trainee1AccessToken: any;
    let trainee2AccessToken: any;
    let trainee3AccessToken: any;
    let traineeUser1: any;
    let traineeUser2: any;
    let traineeUser3: any;
    let coachUser1: any;
    let coachUser2: any;
    let adminUser1: any;
    let noDBTrack: any;
    let TRACK_USER_1: any;

    beforeEach(async () => {
        await User.deleteMany({});
        await UserTrack.deleteMany({});

        const password = '123456';
        // const passwordHashed = password;
        const passwordHashed = await bcrypt.hash(password, 1);

        const ADMIN_1 = {
            email: 'admin@traily.club',
            role: 'admin',
            password: passwordHashed,
            name: 'admin one'
        };

        const COACH_1 = {
            email: 'coach1@traily.club',
            role: 'coach',
            password: passwordHashed,
            name: 'coach one'
        };

        const COACH_2 = {
            email: 'coach2@traily.club',
            role: 'coach',
            password: passwordHashed,
            name: 'coach two'
        };


        await User.insertMany([ADMIN_1, COACH_1, COACH_2]);

        adminUser1 = await User.findOne(ADMIN_1);
        coachUser1 = await User.findOne(COACH_1);
        coachUser2 = await User.findOne(COACH_2);

        const TRAINEE_1 = {
            email: 'trainee1@traily.club',
            role: 'trainee',
            password: passwordHashed,
            name: 'trainee one',
            coach: coachUser1.id
        };

        const TRAINEE_2 = {
            email: 'trainee2@traily.club',
            role: 'trainee',
            password: passwordHashed,
            name: 'trainee two',
            coach: coachUser1.id
        };

        const TRAINEE_3 = {
            email: 'trainee3@traily.club',
            role: 'trainee',
            password: passwordHashed,
            name: 'trainee three',
            coach: coachUser2.id
        };

        await User.insertMany([TRAINEE_1, TRAINEE_2, TRAINEE_3]);
        traineeUser1 = await User.findOne({email: TRAINEE_1.email});
        traineeUser2 = await User.findOne({email: TRAINEE_2.email});
        traineeUser3 = await User.findOne({email: TRAINEE_3.email});


        // await UserTrack.insertMany(tracks);

        TRACK_USER_1 = {
            user: traineeUser1,
            name: `trainee1 track 0`,
            rating: getRandomInt(6),
            locations: generateRandomPoints({'lat': 45.1841602, 'lng': 5.6805232}, 1000, 10)
        };


        noDBTrack = {
            name: 'trainee1 inserted track',
            duration: 4800000,
            locations: generateRandomPoints({'lat': 45.1841602, 'lng': 5.6805232}, 1000, 10)
        };

        ADMIN_1.password = password;
        COACH_1.password = password;
        COACH_2.password = password;
        TRAINEE_1.password = password;
        TRAINEE_2.password = password;
        TRAINEE_3.password = password;

        adminAccessToken = (await User.findAndGenerateToken(ADMIN_1)).accessToken;
        coach1AccessToken = (await User.findAndGenerateToken(COACH_1)).accessToken;
        coach2AccessToken = (await User.findAndGenerateToken(COACH_2)).accessToken;
        trainee1AccessToken = (await User.findAndGenerateToken(TRAINEE_1)).accessToken;
        trainee2AccessToken = (await User.findAndGenerateToken(TRAINEE_2)).accessToken;
        trainee3AccessToken = (await User.findAndGenerateToken(TRAINEE_3)).accessToken;
    });

    describe('GET /api/users/:userId/trainees', () => {
        it('should return the coach\'s trainees', async () => {
            const coachId = coachUser1.id;
            return request(app)
                .get(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.data[0].email).to.be.equal(traineeUser1.email);
                    expect(res.body.data[1].email).to.be.equal(traineeUser2.email);
                });
        });

        it('should return the coach\'s trainees as admin', async () => {
            const coachId = coachUser1.id;
            return request(app)
                .get(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.data[0].email).to.be.equal(traineeUser1.email);
                    expect(res.body.data[1].email).to.be.equal(traineeUser2.email);
                });
        });

        it('should report an error when logged as another coach', async () => {
            const coachId = coachUser1.id;
            return request(app)
                .get(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${coach2AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                });
        });

        it('should report an error when logged as one of the trainees', async () => {
            const coachId = coachUser1.id;
            return request(app)
                .get(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                });
        });
    });

    describe('DELETE /api/users/:userId/trainees', () => {
        it('should remove coach\'s trainee', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;
            return request(app)
                .delete(`/api/users/${coachUser1.id}/trainees`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .send({traineeId: id})
                .expect(httpStatus.NO_CONTENT)
                .then(async () => {
                    const removedTrainee = await User.findById(id);
                    expect(removedTrainee.coach).to.be.null;
                    const numberOfTrainees = await User.find({'coach': new ObjectId(coachUser1.id)});
                    expect(numberOfTrainees.length).to.be.equal(1);
                });
        });

        it('should remove coach\'s trainee as admin', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;
            return request(app)
                .delete(`/api/users/${coachUser1.id}/trainees`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send({traineeId: id})
                .expect(httpStatus.NO_CONTENT)
                .then(async () => {
                    const removedTrainee = await User.findById(id);
                    expect(removedTrainee.coach).to.be.null;
                    const numberOfTrainees = await User.find({'coach': new ObjectId(coachUser1.id)});
                    expect(numberOfTrainees.length).to.be.equal(1);
                });
        });

        it('should report an error when another coach tries to remove a trainee', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;
            return request(app)
                .delete(`/api/users/${coachUser1.id}/trainees`)
                .set('Authorization', `Bearer ${coach2AccessToken}`)
                .send({traineeId: id})
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should report an error when a trainee tries to remove themselves', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;
            return request(app)
                .delete(`/api/users/${coachUser1.id}/trainees`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send({traineeId: id})
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                    expect(res.body.message).to.be.equal('Only the user, their coach and admins have access to this feature');
                });
        });

        it('should report an error when a non logged user tries to remove a trainee', async () => {
            const id = (await User.findOne({'email': traineeUser1.email}))._id;
            return request(app)
                .delete(`/api/users/${coachUser1.id}/trainees`)
                // .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send({traineeId: id})
                .expect(httpStatus.UNAUTHORIZED)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.UNAUTHORIZED);
                });
        });
    });

    describe('POST /api/users/:userId/trainees', () => {

        it('should assign the requested trainee to the coach', async () => {
            const coachId = coachUser1.id;
            const traineeEmail = traineeUser1.email;
            await User.update({'email': traineeUser1.email}, {$unset: {coach: ""}});
            return request(app)
                .post(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .send({email: traineeEmail})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.email).to.be.equal(traineeEmail);
                    expect(res.body.coach).to.be.equal(coachId);
                });
        });

        it('should assign the requested trainee to the coach as admin', async () => {
            const coachId = coachUser1.id;
            const traineeEmail = traineeUser1.email;
            await User.update({'email': traineeUser1.email}, {$unset: {coach: ""}});
            return request(app)
                .post(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send({email: traineeEmail})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.email).to.be.equal(traineeEmail);
                    expect(res.body.coach).to.be.equal(coachId);
                });
        });

        it('should report an error when the trainee is already assigned to a coach', () => {
            const coachId = coachUser1.id;
            const traineeEmail = traineeUser1.email;
            return request(app)
                .post(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${coach1AccessToken}`)
                .send({email: traineeEmail})
                .expect(httpStatus.CONFLICT)
                .then((res: any) => {
                    expect(res.status).to.be.equal(httpStatus.CONFLICT);
                    expect(res.text).to.be.equal('User already assigned to another coach');
                })
        });

        it('should assign the request trainee to the coach as admin if the trainee is already assigned', () => {
            const coachId = coachUser1.id;
            const traineeEmail = traineeUser1.email;
            return request(app)
                .post(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${adminAccessToken}`)
                .send({email: traineeEmail})
                .expect(httpStatus.OK)
                .then((res: any) => {
                    expect(res.body.email).to.be.equal(traineeEmail);
                    expect(res.body.coach).to.be.equal(coachId);
                });
        });

        it('should report an error when the trainee tries to remove their coach', async () => {
            const coachId = coachUser1.id;
            const traineeEmail = traineeUser1.email;
            await User.update({'email': traineeUser1.email}, {$unset: {coach: ""}});
            return request(app)
                .post(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${trainee1AccessToken}`)
                .send({email: traineeEmail})
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                })
        });

        it('should report an error when another coach tries to remove a trainee', async () => {
            const coachId = coachUser1.id;
            const traineeEmail = traineeUser1.email;
            await User.update({'email': traineeUser1.email}, {$unset: {coach: ""}});
            return request(app)
                .post(`/api/users/${coachId}/trainees`)
                .set('Authorization', `Bearer ${coach2AccessToken}`)
                .send({email: traineeEmail})
                .expect(httpStatus.FORBIDDEN)
                .then((res: any) => {
                    expect(res.body.code).to.be.equal(httpStatus.FORBIDDEN);
                })
        });

    });
});
