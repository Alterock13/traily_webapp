import * as path from 'path';
import {NextFunction, Request, Response} from 'express';
import {User, UserTrack} from '../models';
import {apiJson, startTimer} from '../utils/Utils';
import {existsSync, unlink} from 'fs';

export {};

const mongoose = require('mongoose');
const sanitize = require('mongo-sanitize');
const ObjectId = mongoose.Types.ObjectId;
const httpStatus = require('http-status');

const {omit} = require('lodash');
const uploadsFolder = path.resolve(`${__dirname}/../../../uploads`);

const {handler: errorHandler} = require('../middlewares/error');

/**
 * Load user and append to req.
 * @public
 */
exports.load = async (req: Request, res: Response, next: NextFunction) => {
    if (req.route === undefined) {
        return next();
    }
    try {
        const user = await User.get(sanitize(req.params.userId));
        req.route.meta = req.route.meta || {};
        req.route.meta.user = user;
        return next();
    } catch (error) {
        return errorHandler(error, req, res);
    }
};

/**
 * Get user
 * @public
 */
exports.get = async (req: Request, res: Response) => {
    try {
        const user = await User.findById(sanitize(req.params.userId));
        res.status(httpStatus.OK);
        res.json(user.transform());
    } catch (error) {
        return errorHandler(error, req, res);
    }
};

/**
 * Get logged in user info
 * @public
 */
exports.loggedIn = (req: Request, res: Response) => res.json(req.route.meta.user.transform());

/**
 * Create new user
 * @public
 */
exports.create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = new User(sanitize(req.body));
        const savedUser = await user.save();
        res.status(httpStatus.CREATED);
        res.json(savedUser.transform());
    } catch (error) {
        next(User.checkDuplicateEmail(error));
    }
};

/**
 * Replace existing user
 * @public
 */
exports.replace = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = await User.findById(sanitize(req.params.userId));
        const newUser = new User(sanitize(req.body));
        const omitRole = user.role !== 'admin' ? 'role' : '';
        const newUserObject = omit(newUser.toObject(), '_id', omitRole);
        await user.update(newUserObject, {override: true, upsert: true});
        const savedUser = await User.findById(user._id);

        res.json(savedUser.transform());
    } catch (error) {
        next(User.checkDuplicateEmail(error));
    }
};

/**
 * Update existing user
 * @public
 */
exports.update = async (req: Request, res: Response, next: NextFunction) => {
    const requestedUser = await User.findById(sanitize(req.params.userId));
    const omitRole = req.route.meta.user.role !== 'admin' ? 'role' : '';
    const updatedUser = omit(req.body, omitRole);
    const user = Object.assign(requestedUser, updatedUser);

    user
        .save()
        .then((savedUser: any) => res.json(savedUser.transform()))
        .catch((e: any) => next(User.checkDuplicateEmail(e)));
};

/**
 * Get user list
 * @public
 * @example GET https://localhost:3009/api/users?role=admin&limit=5&offset=0&sort=email:desc,createdAt
 */
exports.list = async (req: Request, res: Response, next: NextFunction) => {
    try {
        startTimer(req);
        const data = (await User.list(sanitize(req))).transform(req);
        apiJson({req, res, data, model: User});
    } catch (e) {
        next(e);
    }
};


/**
 * Delete user
 * @public
 */
exports.remove = (req: Request, res: Response, next: NextFunction) => {
    const userId = sanitize(req.params.userId);
    UserTrack.deleteMany({'user': userId}).exec();
    User.updateMany({'coach': userId}, {'$unset': {'coach': ''}}).exec();
    User
        .deleteOne({'_id': new ObjectId(userId)})
        .then(async () => {
            if (await existsSync(`${uploadsFolder}/${userId}.png`)) {
                await unlink(`${uploadsFolder}/${userId}.png`, err => {
                    if (err) {
                        throw err;
                    }
                });
            }

        })
        .then(() => res.status(httpStatus.NO_CONTENT).end())
        .catch((e: any) => next(e));
};


/**
 * Upload image
 * @public
 */
exports.upload = async (req: Request, res: Response, next: NextFunction) => {
    const userId = sanitize(req.params.userId);
    try {
        if (await !existsSync(`${uploadsFolder}/${userId}.png`)) {
            res
                .status(httpStatus.BAD_REQUEST)
                .send('Looks like the upload failed, make sure you selected a file to upload')
                .end();
        } else {
            res.sendFile(`${uploadsFolder}/${userId}.png`);
        }
    } catch (error) {
        return next(error);
    }
};

/**
 * Get user image
 * @public
 */
exports.getImage = async (req: Request, res: Response, next: NextFunction) => {
    const userId = sanitize(req.params.userId);
    try {
        if (await !existsSync(`${uploadsFolder}/${userId}.png`)) {
            res
                .status(httpStatus.NOT_FOUND)
                .send('User does not have an image')
                .end();
        } else {
            res.sendFile(`${uploadsFolder}/${userId}.png`);
        }
    } catch (error) {
        return next(error);
    }
};

/**
 * Delete user image
 * @public
 */
exports.deleteImage = async (req: Request, res: Response, next: NextFunction) => {
    const userId = sanitize(req.params.userId);
    if (userId !== req.route.meta.user.id && req.route.meta.user.role !== 'admin') {
        res
            .status(httpStatus.FORBIDDEN)
            .send('Only image owner and admins can delete images')
            .end();
    }
    try {
        if (await !existsSync(`${uploadsFolder}/${userId}.png`)) {
            res
                .status(httpStatus.NOT_FOUND)
                .send('User does not have an image')
                .end();
        }
        await unlink(`${uploadsFolder}/${userId}.png`, err => {
            if (err) {
                throw err;
            }
            // if no error, file has been deleted successfully
            res
                .status(httpStatus.NO_CONTENT)
                .end();
        });
    } catch (error) {
        return next(error);
    }
};

