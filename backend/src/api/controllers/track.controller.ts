export {};
import {NextFunction, Request, Response} from 'express';

const mongoose = require('mongoose');
const sanitize = require('mongo-sanitize');
const ObjectId = mongoose.Types.ObjectId;
const httpStatus = require('http-status');
import {User, UserTrack} from '../models';
import {startTimer, apiJson, computeDistance} from '../utils/Utils';

const {handler: errorHandler} = require('../middlewares/error');

/**
 * Load track and append to req.
 * @public
 */
exports.load = async (req: Request, res: Response, next: NextFunction) => {

    try {
        const track = await UserTrack.get(sanitize(req.params.trackId));
        req.route.meta = req.route.meta || {};
        req.route.meta.track = track;
        return next();
    } catch (error) {
        return errorHandler(error, req, res);
    }
};

/**
 * Get track
 * @public
 */
exports.get = async (req: Request, res: Response) => {
    try {
        const track = await UserTrack.findById(sanitize(req.params.trackId));
        res.status(httpStatus.OK);
        res.json(track.transform());
    } catch (error) {
        return errorHandler(error, req, res);
    }
};

/**
 * Create new track
 * @public
 */
exports.create = async (req: Request, res: Response) => {
    try {
        const userId = sanitize(req.params.userId);
        const locations = sanitize(req.body.locations);
        req.body.user = userId;
        if (userId !== req.route.meta.user.id && req.route.meta.user.role !== 'admin') {
            res
                .status(httpStatus.FORBIDDEN)
                .send('Only the user and admins have access to this feature')
                .end();
        } else {
            const distance = computeDistance(locations);
            req.body.distance = distance.toFixed(2);
            const track = new UserTrack(sanitize(req.body));
            const savedUserTrack = await track.save();
            res.status(httpStatus.CREATED);
            res.json(savedUserTrack.transform());
        }
    } catch (error) {
        return errorHandler(error, req, res);
    }
};

/**
 * Update existing track
 * @public
 */
exports.update = async (req: Request, res: Response, next: NextFunction) => {
    const loggedUser = req.route.meta.user;
    const requestedUser = await User.findById(sanitize(req.params.userId));
    const coachId = requestedUser.coach ? requestedUser.coach._id.toString() : undefined;
    if (requestedUser.id !== loggedUser.id && coachId !== loggedUser.id && loggedUser.role !== 'admin') {
        res.status(httpStatus.FORBIDDEN).end();
    } else {
        const rating = sanitize(req.body.rating);
        const trackName = sanitize(req.body.name);
        if (loggedUser.role === 'trainee' && rating) {
            res
                .status(httpStatus.FORBIDDEN)
                .send('Users cannot rate their own tracks')
                .end();
        } else if (loggedUser.role === 'coach' && loggedUser.id === requestedUser.id && rating) {
            res
                .status(httpStatus.FORBIDDEN)
                .send('Coaches cannot rate their owns tracks')
                .end();
        } else if (loggedUser.role === 'coach' && trackName) {
            res
                .status(httpStatus.FORBIDDEN)
                .send('Coaches cannot change their trainees tracks names')
                .end();
        } else {
            const track = Object.assign(req.route.meta.track, sanitize(req.body));
            track
                .save()
                .then((savedTrack: any) => res.json(savedTrack.transform()))
                .catch((e: any) => next(errorHandler(e)));
        }
    }
};

/**
 * Get user's tracks.
 * @public
 * @example GET https://localhost:3009/api/users/USERID/tracks
 */
exports.list = async (req: Request, res: Response, next: NextFunction) => {
    try {
        startTimer(req);
        const userId = req.params.userId;
        req.query = {...req.query, user: new ObjectId(userId)}; // append to query (by userId) to final query
        const data = (await UserTrack.list({query: req.query})).transform(req);
        apiJson({req, res, data, model: UserTrack});
    } catch (e) {
        next(e);
    }
};

/**
 * Delete user track
 * @public
 */
exports.remove = async (req: Request, res: Response, next: NextFunction) => {
    const {userId, trackId} = sanitize(req.params);
    const {_id} = req.route.meta.user;
    const currentUserId = _id.toString();
    if (userId !== currentUserId && req.route.meta.user.role !== 'admin') {
        res.status(httpStatus.FORBIDDEN).end();
    } else {
        UserTrack
            .deleteOne({_id: new ObjectId(trackId)})
            .then(() => res.status(httpStatus.NO_CONTENT).end())
            .catch((e: any) => next(e));
    }
};

/**
 * Delete all user's tracks
 * @public
 */
exports.removeAll = async (req: Request, res: Response, next: NextFunction) => {
    const {userId} = sanitize(req.params);
    UserTrack
        .deleteMany({user: new ObjectId(userId)})
        .then(() => res.status(httpStatus.NO_CONTENT).end())
        .catch((e: any) => next(e));
};
