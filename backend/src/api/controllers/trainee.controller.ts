export {};
import {NextFunction, Request, Response} from 'express';

const mongoose = require('mongoose');
const sanitize = require('mongo-sanitize');
const ObjectId = mongoose.Types.ObjectId;
const httpStatus = require('http-status');
import {User} from '../models';
import {startTimer, apiJson} from '../utils/Utils';

const {handler: errorHandler} = require('../middlewares/error');

/**
 * Assign trainee to coach
 * @public
 */
exports.assign = async (req: Request, res: Response, next: NextFunction) => {
    const trainee = await User.findOne({'email': sanitize(req.body.email)});
    if (!trainee) {
        res
            .status(httpStatus.FORBIDDEN)
            .send('User does not exist')
            .end();
    }
    const coachId = sanitize(req.params.userId);
    if (!trainee.coach || req.route.meta.user.role === 'admin') {
        const user = Object.assign(trainee, {'coach': coachId});
        user
            .save()
            .then((savedUser: any) => res.json(savedUser.transform()))
            .catch((e: any) => next(errorHandler(e)));
    } else {
        res
            .status(httpStatus.CONFLICT)
            .send('User already assigned to another coach')
            .end();
    }
};

/**
 * Get coach's trainees.
 * @public
 * @example GET https://localhost:3009/api/users/USERID/trainees
 */
exports.list = async (req: Request, res: Response, next: NextFunction) => {
    try {
        startTimer(req);
        const userId = sanitize(req.params.userId);
        req.query = {...req.query, coach: new ObjectId(userId)}; // append to query (by userId) to final query
        const data = (await User.list({query: sanitize(req.query)})).transform(req);
        apiJson({req, res, data, model: User});
    } catch (e) {
        next(e);
    }
};

/**
 * Delete user track
 * @public
 */
exports.remove = async (req: Request, res: Response, next: NextFunction) => {
    const traineeId = sanitize(req.body.traineeId);
    User.findOneAndUpdate({'_id': traineeId}, {'coach': null})
        .exec()
        .then(() => res.status(httpStatus.NO_CONTENT).end())
        .catch((e: any) => next(e));
};
