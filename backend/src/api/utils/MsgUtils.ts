const fs = require('fs');


// configure for emailing
const {EMAIL_MAILGUN_API_KEY, EMAIL_FROM_SUPPORT, EMAIL_MAILGUN_DOMAIN, EMAIL_TEMPLATE_BASE} = require('config/vars');
const handlebars = require('handlebars');

// load template file & inject data => return content with injected data.
const template = (fileName: string, data: any) => {
    const content = fs.readFileSync(EMAIL_TEMPLATE_BASE + fileName).toString();
    const inject = handlebars.compile(content);
    return inject(data);
};

// --------- Email Templates --------- //

export function welcomeEmail({name, email}: { name: string; email: string }) {
    return {
        from: EMAIL_FROM_SUPPORT,
        to: `${name} <${email}>`,
        subject: `Welcome!`,
        text: template('welcome.txt', {name, email}),
        html: template('welcome.html', {name, email})
    };
}

export function forgotPasswordEmail({name, email, tempPass}: { name: string; email: string; tempPass: string }) {
    return {
        from: EMAIL_FROM_SUPPORT,
        to: `${name} <${email}>`,
        subject: `Your one-time temporary password`,
        text: template('forgot-password.txt', {name, email, tempPass}),
        html: template('forgot-password.html', {name, email, tempPass})
    };
}

// resetPswEmail, forgotPswEmail, etc.

// --------- Nodemailer and Mailgun setup --------- //

const mailgun = require('mailgun-js')({apiKey: EMAIL_MAILGUN_API_KEY, domain: EMAIL_MAILGUN_DOMAIN});

export function sendEmail(data: any) {
    if (EMAIL_MAILGUN_API_KEY) {
        return new Promise((resolve, reject) => {
            mailgun.messages().send(data, (err: any, info: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(info);
                }
            });
        });
    }
}
