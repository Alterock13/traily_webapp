import * as path from 'path';
import {Request, Response} from 'express';
import {ITEMS_PER_PAGE} from './Const';
import {existsSync} from 'fs';

const mstime = require('mstime');
const fs = require('fs');
const request = require('request');
const sharp = require('sharp');


const uploadsFolder = path.resolve(`${__dirname}/../../../uploads`);

export function jsonClone(obj: any) {
    return JSON.parse(JSON.stringify(obj));
}

// Helper functions for Utils.uuid()
const lut = Array(256)
    .fill('')
    .map((_, i) => (i < 16 ? '0' : '') + i.toString(16));
const formatUuid = ({d0, d1, d2, d3}: { d0: number; d1: number; d2: number; d3: number }) =>
    // tslint:disable-next-line:prefer-template
    lut[d0 & 0xff] +
    lut[(d0 >> 8) & 0xff] +
    lut[(d0 >> 16) & 0xff] +
    lut[(d0 >> 24) & 0xff] +
    '-' +
    lut[d1 & 0xff] +
    lut[(d1 >> 8) & 0xff] +
    '-' +
    lut[((d1 >> 16) & 0x0f) | 0x40] +
    lut[(d1 >> 24) & 0xff] +
    '-' +
    lut[(d2 & 0x3f) | 0x80] +
    lut[(d2 >> 8) & 0xff] +
    '-' +
    lut[(d2 >> 16) & 0xff] +
    lut[(d2 >> 24) & 0xff] +
    lut[d3 & 0xff] +
    lut[(d3 >> 8) & 0xff] +
    lut[(d3 >> 16) & 0xff] +
    lut[(d3 >> 24) & 0xff];

const getRandomValuesFunc =
    typeof window !== 'undefined' && window.crypto && window.crypto.getRandomValues
        ? () => {
            const dvals = new Uint32Array(4);
            window.crypto.getRandomValues(dvals);
            return {
                d0: dvals[0],
                d1: dvals[1],
                d2: dvals[2],
                d3: dvals[3]
            };
        }
        : () => ({
            d0: (Math.random() * 0x100000000) >>> 0,
            d1: (Math.random() * 0x100000000) >>> 0,
            d2: (Math.random() * 0x100000000) >>> 0,
            d3: (Math.random() * 0x100000000) >>> 0
        });

/* -------------------------------------------------------------------------------- */

export function uuid() {
    return formatUuid(getRandomValuesFunc());
}

export function startTimer(req: Request) {
    mstime.start(req.originalUrl, {uuid: uuid()});
}

export function getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max));
}

export function endTimer(req: Request) {
    const end = mstime.end(req.originalUrl);
    if (end) {
        console.log(`avg time - ${end.avg} (ms)`);
        return end;
    }
    return null;
}

// from "sort" string (URL param) => build sort object (mongoose), e.g. "sort=name:desc,age"
export function getSortQuery(sortStr: string, defaultKey = 'createdAt') {
    let arr = [sortStr || defaultKey];
    if (sortStr && sortStr.indexOf(',')) {
        arr = sortStr.split(',');
    }
    let ret = {};
    for (let i = 0; i < arr.length; i += 1) {
        let order = 1; // default: ascending (a-z)
        let keyName = arr[i].trim();
        if (keyName.indexOf(':') >= 0) {
            const [keyStr, orderStr] = keyName.split(':'); // e.g. "name:desc"
            keyName = keyStr.trim();
            order = orderStr.trim() === 'desc' || orderStr.trim() === '-1' ? -1 : 1;
        }
        ret = {...ret, [keyName]: order};
    }
    return ret;
}

// from "req" (req.query) => transform to: query object, e.g. { limit: 5, sort: { name: 1 } }
export function getPageQuery(reqQuery: any) {
    if (!reqQuery) {
        return null;
    }
    const output: any = {};
    if (reqQuery.page) {
        output.perPage = reqQuery.perPage || ITEMS_PER_PAGE; // if page is set => take (or set default) perPage
    }
    if (reqQuery.fields) {
        output.fields = reqQuery.fields.split(',').map((field: string) => field.trim()); // to array
    }
    // number (type) query params => parse them:
    const numParams = ['page', 'perPage', 'limit', 'offset'];
    numParams.forEach(field => {
        if (reqQuery[field]) {
            output[field] = parseInt(reqQuery[field], 10);
        }
    });
    output.sort = getSortQuery(reqQuery.sort, 'createdAt');
    return output;
}

// normalize req.query to get "safe" query fields => return "query" obj for mongoose (find, etc.)
export function getQuery(reqQuery: any, fieldArray: string[]) {
    const queryObj: any = {};
    fieldArray.map(field => {
        // get query fields excluding pagination fields:
        if (['page', 'perPage', 'limit', 'offset'].indexOf(field) < 0 && reqQuery[field]) {
            // TODO: do more checks of query parameters for better security...
            let val = reqQuery[field];
            if (typeof val === 'string' && val.length >= 2 && (val[0] === '*' || val[val.length - 1] === '*')) {
                // field value has "*text*" => use MongoDB Regex query: (partial text search)
                val = val.replace(/\*/g, ''); // remove "*"
                val = val.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // escape other special chars - https://goo.gl/eWCVDH
                queryObj[field] = {$regex: val, $options: 'i'};
            } else {
                queryObj[field] = reqQuery[field]; // exact search
            }
        }
    });
    console.log('- queryObj: ', JSON.stringify(queryObj));
    return queryObj;
}

// function to decorate a promise with useful helpers like: .transform(), etc.
// @example: return queryPromise( this.find({}) )
export function queryPromise(mongoosePromise: any) {
    return new Promise(async resolve => {
        const items = await mongoosePromise;

        // decorate => transform() on the result
        items.transform = (params: any) => {
            return items.map((item: any) => (item.transform ? item.transform(params) : item));
        };
        resolve(items);
    });
}

type apiJsonTypes = {
    req: Request;
    res: Response;
    data: any | any[]; // data can be object or array
    model?: any; // e.g. "listModal: User" to get meta.totalCount (User.countDocuments())
    meta?: any;
    json?: boolean; // retrieve JSON only (won't use res.json(...))
};

/**
 * prepare a standard API Response, e.g. { meta: {...}, data: [...], errors: [...] }
 * @param param0
 */
export async function apiJson({req, res, data, model, meta = {}, json = false}: apiJsonTypes) {
    const queryObj = getPageQuery(req.query);
    const metaData = {...queryObj, ...meta};

    if (model) {
        // if pass in "model" => query for totalCount & put in "meta"
        const isPagination = req.query.limit || req.query.page;
        if (isPagination && model.countDocuments) {
            const query = getQuery(req.query, model.ALLOWED_FIELDS);
            const countQuery = jsonClone(query);
            const totalCount = await model.countDocuments(countQuery);
            metaData.totalCount = totalCount;
            if (queryObj.perPage) {
                metaData.pageCount = Math.ceil(totalCount / queryObj.perPage);
            }
            metaData.count = data && data.length ? data.length : 0;
        }
    }
    // add Timer data
    const timer = endTimer(req);
    if (timer) {
        metaData.timer = timer.last;
        metaData.timerAvg = timer.avg;
    }

    const output = {data, meta: metaData};
    if (json) {
        return output;
    }
    return res.json(output);
}

export function randomString(len = 10, charStr = 'abcdefghijklmnopqrstuvwxyz0123456789') {
    const chars = [...`${charStr}`];
    return [...Array(len)].map(() => chars[(Math.random() * chars.length) | 0]).join('');
}

/**
 * Generates number of random geolocation points given a center and a radius.
 * @param  {Object} center A JS object with lat and lng attributes.
 * @param  {number} radius Radius in meters.
 * @param {number} count Number of points to generate.
 * @return {array} Array of Objects with lat and lng attributes.
 */
export function generateRandomPoints(center: any, radius: number, count: number) {
    const points = [];
    for (let i = 0; i < count; i += 1) {
        points.push(generateRandomPoint(center, radius));
    }
    return points;
}


/**
 * Generates number of random geolocation points given a center and a radius.
 * Reference URL: http://goo.gl/KWcPE.
 * @param  {Object} center A JS object with lat and lng attributes.
 * @param  {number} radius Radius in meters.
 * @return {Object} The generated random points as JS object with lat and lng attributes.
 */
function generateRandomPoint(center: any, radius: number) {
    const x0 = center.lng;
    const y0 = center.lat;
    // Convert Radius from meters to degrees.
    const rd = radius / 111300;

    const u = Math.random();
    const v = Math.random();

    const w = rd * Math.sqrt(u);
    const t = 2 * Math.PI * v;
    const x = w * Math.cos(t);
    const y = w * Math.sin(t);

    const xp = x / Math.cos(y0);

    // Resulting point.
    // return { 'lat': y + y0, 'lng': xp + x0 };
    return [xp + x0, y + y0];
}

/**
 * Generates a random date between two dates.
 * @param  {Date} start date.
 * @param  {Date} end date.
 * @return {Date} randomly generated date between start and end dates.
 */
export function generateRandomDate(start: Date, end: Date) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function degreesToRadians(degrees: number) {
    return degrees * Math.PI / 180;
}

function distanceInKmBetweenEarthCoordinates(lon1: number, lat1: number, lon2: number, lat2: number) {
    const earthRadiusKm = 6371;

    const dLat = degreesToRadians(lat2 - lat1);
    const dLon = degreesToRadians(lon2 - lon1);

    const rLat1 = degreesToRadians(lat1);
    const rLat2 = degreesToRadians(lat2);

    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(rLat1) * Math.cos(rLat2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return earthRadiusKm * c;
}

/**
 * Compute the distance between the start and end gps coords
 * @param {array} locations
 * @return {number} calculated distance
 * */
export function computeDistance(locations: any): number {
    const locationsToShift = Object.assign([], locations);
    if (locationsToShift.length < 2) {
        return 0;
    }
    if (locationsToShift.length === 2) {
        return distanceInKmBetweenEarthCoordinates(locationsToShift[0][0], locationsToShift[0][1], locationsToShift[1][0], locationsToShift[1][1]);
    }
    const distance = distanceInKmBetweenEarthCoordinates(locationsToShift[0][0], locationsToShift[0][1], locationsToShift[1][0], locationsToShift[1][1]);
    locationsToShift.shift();
    return distance + computeDistance(locationsToShift);
}

/**
 * Download and store an image as PNG from an URI
 * @param facebookID
 * @param userID
 * */
export async function downloadFBImage(facebookID: string, userID: any) {
    const input = `${uploadsFolder}/${userID}.jpeg`;
    const output = `${uploadsFolder}/${userID}.png`;
    const options = {
        url: `https://graph.facebook.com/${facebookID}/picture?type=square&width=200&height=200`
    };
    request.get(options, async (err: any, res: { request: { uri: { href: any; }; }; }) => {
        const fileStream = fs.createWriteStream(input);
        const download = request(res.request.uri.href)
            .pipe(fileStream);
        download.on('finish', async () => {
            if (existsSync(input)) {
                await sharp(input)
                    .toFile(output);
            }
        });
    });
}
