import {computeDistance, generateRandomDate, generateRandomPoints, getRandomInt} from './Utils';

export {};
import {User, UserTrack} from '../models';

const ADMIN_USER_1 = {
    email: 'admin1@traily.club',
    role: 'admin',
    password: '1admin1',
    name: 'admin one'
};
const ADMIN_USER_2 = {
    email: 'admin2@traily.club',
    role: 'admin',
    password: '2admin2',
    name: 'admin two'
};


const COACH_1 = {
    email: 'coach1@traily.club',
    role: 'coach',
    password: '1coach1',
    name: 'coach one'
};

const COACH_2 = {
    email: 'coach2@traily.club',
    role: 'coach',
    password: '2coach2',
    name: 'coach two'
};


async function setup() {
    const adminUser1 = new User(ADMIN_USER_1);
    await adminUser1.save();

    const adminUser2 = new User(ADMIN_USER_2);
    await adminUser2.save();

    const coachUser1 = new User(COACH_1);
    await coachUser1.save();

    const coachUser2 = new User(COACH_2);
    await coachUser2.save();


    const TRAINEE_1 = {
        email: 'trainee1@traily.club',
        role: 'trainee',
        password: '1trainee1',
        name: 'trainee one',
        coach: coachUser1
    };

    const TRAINEE_2 = {
        email: 'trainee2@traily.club',
        role: 'trainee',
        password: '2trainee2',
        name: 'trainee two',
        coach: coachUser2
    };

    const traineeUser1 = new User(TRAINEE_1);
    await traineeUser1.save();

    const traineeUser2 = new User(TRAINEE_2);
    await traineeUser2.save();

    // Add trainees
    let lat = 45.1841602;
    let lng = 5.6805232;
    for (let i = 0; i < 10; i += 1) {
        lat += 0.02;
        lng += 0.02;
        const geoLocs = generateRandomPoints({'lat': lat, 'lng': lng}, 1000, 10);
        const geoDistance = computeDistance(geoLocs);
        await new UserTrack({
            user: traineeUser1,
            name: `trainee1 track ${i}`,
            rating: getRandomInt(6),
            locations: geoLocs,
            distance: geoDistance,
            createdAt: generateRandomDate(new Date(2019, 3, 1), new Date())
        }).save();

        await new UserTrack({
            user: traineeUser2,
            name: `trainee2 track ${i}`,
            rating: getRandomInt(6),
            locations: geoLocs,
            distance: geoDistance,
            createdAt: generateRandomDate(new Date(2019, 3, 1), new Date())
        }).save();

        await new UserTrack({
            user: coachUser1,
            name: `coach1 track ${i}`,
            rating: getRandomInt(6),
            locations: geoLocs,
            distance: geoDistance,
            createdAt: generateRandomDate(new Date(2019, 3, 1), new Date())
        }).save();

        await new UserTrack({
            user: coachUser2,
            name: `coach2 track ${i}`,
            rating: getRandomInt(6),
            locations: geoLocs,
            distance: geoDistance,
            createdAt: generateRandomDate(new Date(2019, 3, 1), new Date())
        }).save();

        await new UserTrack({
            user: adminUser1,
            name: `admin1 track ${i}`,
            rating: getRandomInt(6),
            locations: geoLocs,
            distance: geoDistance,
            createdAt: generateRandomDate(new Date(2019, 3, 1), new Date())
        }).save();

        await new UserTrack({
            user: adminUser2,
            name: `admin2 track ${i}`,
            rating: getRandomInt(6),
            locations: geoLocs,
            distance: geoDistance,
            createdAt: generateRandomDate(new Date(2019, 3, 1), new Date())
        }).save();
    }

}

async function checkNewDB() {
    const adminUser1 = await User.findOne({email: ADMIN_USER_1.email});
    if (!adminUser1) {
        console.log('- New DB detected ===> Initializing Dev Data...');
        await User.deleteMany({});
        await UserTrack.deleteMany({});
        await setup();
    } else {
        console.log('- Skip InitData');
    }
}

checkNewDB();
