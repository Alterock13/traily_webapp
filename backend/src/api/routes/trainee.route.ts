export {};
const validate = require('express-validation');
const controller = require('../controllers/trainee.controller');
const {authorize, COACH} = require('../middlewares/auth');
const {listTrainees, assignTrainee, removeTrainee} = require('../validations/trainee.validation');

const trainees = require('express').Router({mergeParams: true});

trainees
    .route('/')
    /**
     * @api {get} users/:userid/trainees List coach's trainees
     * @apiDescription Get a list of coach's trainees
     * @apiVersion 1.0.0
     * @apiName ListTrainees
     * @apiGroup Trainee
     * @apiPermission coach, admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {Number}             [userID]       User's ID
     *
     * @apiSuccess {Object[]} tracks List of coach's trainees.
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admins and the user's coach can access the data
     */
    .get(authorize(COACH), validate(listTrainees), controller.list)
    /**
     * @api {delete} users/:userid/trainees Remove coach's trainee
     * @apiDescription Remove coach's trainee
     * @apiVersion 1.0.0
     * @apiName RemoveTrainee
     * @apiGroup Trainee
     * @apiPermission coach, admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiSuccess (No Content 204)  Successfully deleted
     *
     * @apiError (Unauthorized 401) Unauthorized  Only authenticated users can delete the data
     * @apiError (Forbidden 403)    Forbidden     Only user with same id, user's coach and admins can delete the data
     * @apiError (Not Found 404)    NotFound      User does not exist

     */
    .delete(authorize(COACH), validate(removeTrainee), controller.remove)

    /**
     * @api {post} users/:userid/trainees Assign trainee to coach
     * @apiDescription Assign trainee to coach
     * @apiVersion 1.0.0
     * @apiName AssignTrainee
     * @apiGroup Trainee
     * @apiPermission coach, admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {String}                  name            Track's name
     * @apiParam  {Object[]}                locations       Track's duration
     * @apiParam  {Number}                  duration        Track's duration
     *
     * @apiSuccess {String}  id         User's id
     * @apiSuccess {String}  name       User's name
     * @apiSuccess {String}  email      User's email
     * @apiSuccess {String}  role       User's role
     * @apiSuccess {String}  coach      User's coach
     * @apiSuccess {Date}    createdAt  Timestamp
     *
     * @apiError (Bad Request 400)   ValidationError  Some parameters may contain invalid values
     * @apiError (Unauthorized 401)  Unauthorized     Only authenticated users can assign the trainee
     * @apiError (Forbidden 403)     Forbidden        Only admins can assign the data
     * @apiError (Not Found 404)    NotFound      User does not exist
     */
    .post(authorize(COACH), validate(assignTrainee), controller.assign);

module.exports = trainees;
