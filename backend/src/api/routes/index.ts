export {};
import * as express from 'express';
import {apiJson} from '../utils/Utils';

const userRoutes = require('./user.route');
const authRoutes = require('./auth.route');

const router = express.Router();

/**
 * GET status
 */
router.get('/status', (req, res, next) => {
    apiJson({req, res, data: {status: 'OK'}});
    // return next();
});

/**
 * GET docs
 */
router.use('/docs', express.static('docs'));

router.use('/users', userRoutes);
router.use('/auth', authRoutes);

module.exports = router;
