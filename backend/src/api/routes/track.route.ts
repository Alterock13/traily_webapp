export {};
const validate = require('express-validation');
const controller = require('../controllers/track.controller');
const {authorize, LOGGED_USER} = require('../middlewares/auth');
const {listTracks, createTrack, updateTrack} = require('../validations/track.validation');

const tracks = require('express').Router({mergeParams: true});

/**
 * Load track when API with userId route parameter is hit
 */
tracks.param('trackId', controller.load);


tracks
    .route('/')
    /**
     * @api {get} users/:userid/tracks List user's tracks
     * @apiDescription Get a list of user's tracks
     * @apiVersion 1.0.0
     * @apiName ListTracks
     * @apiGroup Track
     * @apiPermission user, user's coach, admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {Number}             [userID]       User's ID
     *
     * @apiSuccess {Object[]} tracks List of tracks.
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admins and the user's coach can access the data
     */
    .get(authorize(LOGGED_USER), validate(listTracks), controller.list)
    /**
     * @api {delete} users/:userid/tracks Delete tracks
     * @apiDescription Delete all user's tracks
     * @apiVersion 1.0.0
     * @apiName DeleteTracks
     * @apiGroup Track
     * @apiPermission user, user's coach, admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiSuccess (No Content 204)  Successfully deleted
     *
     * @apiError (Unauthorized 401) Unauthorized  Only authenticated users can delete the data
     * @apiError (Forbidden 403)    Forbidden     Only user with same id, user's coach and admins can delete the data
     * @apiError (Not Found 404)    NotFound      User does not exist
     */
    .delete(authorize(LOGGED_USER), controller.removeAll)
    /**
     * @api {post} users/:userid/tracks Create track
     * @apiDescription Create a new track
     * @apiVersion 1.0.0
     * @apiName CreateTrack
     * @apiGroup Track
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {String}                  name            Track's name
     * @apiParam  {Object[]}                locations       Track's duration
     * @apiParam  {Number}                  duration        Track's duration
     *
     * @apiSuccess {String}     id          Track's id
     * @apiSuccess {String}     user        Track's owner id
     * @apiSuccess {String}     name        Track's name
     * @apiSuccess {String}     rating      Track's rating
     * @apiSuccess {Number}     duration    Track's duration
     * @apiSuccess {Object[]}   locations   List of locations
     * @apiSuccess {Date}       createdAt   Timestamp
     *
     * @apiError (Bad Request 400)   ValidationError  Some parameters may contain invalid values
     * @apiError (Unauthorized 401)  Unauthorized     Only authenticated users can create the data
     * @apiError (Forbidden 403)     Forbidden        Only admins can create the data
     */
    .post(authorize(LOGGED_USER), validate(createTrack), controller.create);

tracks
    .route('/:trackId')
    /**
     * @api {get} users/:userid/tracks/:trackid Get track
     * @apiDescription Get track information
     * @apiVersion 1.0.0
     * @apiName GetTrack
     * @apiGroup Track
     * @apiPermission user, user's coach, admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {Number}             [userID]       User's ID
     * @apiParam  {Number}             [trackID]       Track's ID
     *
     * @apiSuccess {String}     id          Track's id
     * @apiSuccess {String}     user        Track's owner id
     * @apiSuccess {String}     name        Track's name
     * @apiSuccess {String}     rating      Track's rating
     * @apiSuccess {Object[]}   locations   List of locations
     * @apiSuccess {Date}       createdAt   Timestamp
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admins and the user's coach can access the data
     */
    .get(authorize(LOGGED_USER), controller.get)
    /**
     * @api {patch} /users/:userid/tracks/:trackid Update track
     * @apiDescription Update track information
     * @apiVersion 1.0.0
     * @apiName UpdateTrack
     * @apiGroup Track
     * @apiPermission user, admin
     *
     * @apiHeader   {String}    Authorization   User's access token
     *
     * @apiParam    {String}    [name]          Track's name
     *
     * @apiSuccess {String}     id          Track's id
     * @apiSuccess {String}     user        Track's owner id
     * @apiSuccess {String}     name        Track's name
     * @apiSuccess {String}     rating      Track's rating
     * @apiSuccess {Object[]}   locations   List of locations
     * @apiSuccess {Date}       createdAt   Timestamp
     *
     * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
     * @apiError (Unauthorized 401) Unauthorized Only authenticated users can modify the data
     * @apiError (Forbidden 403)    Forbidden    Only user with same id or admins can modify the data
     * @apiError (Not Found 404)    NotFound     User does not exist
     */
    .patch(authorize(LOGGED_USER), validate(updateTrack), controller.update)
    /**
     * @api {delete} users/:userid/tracks Delete track
     * @apiDescription Delete user's track
     * @apiVersion 1.0.0
     * @apiName DeleteTrack
     * @apiGroup Track
     * @apiPermission user, user's coach, admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiSuccess (No Content 204)  Successfully deleted
     *
     * @apiError (Unauthorized 401) Unauthorized  Only authenticated users can delete the data
     * @apiError (Forbidden 403)    Forbidden     Only user with same id, user's coach and admins can delete the data
     * @apiError (Not Found 404)    NotFound      User does not exist
     */
    .delete(authorize(LOGGED_USER), controller.remove);

module.exports = tracks;
