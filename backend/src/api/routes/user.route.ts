import * as path from 'path';

export {};
const validate = require('express-validation');
const controller = require('../controllers/user.controller');
const multer = require('multer');

const bodyParser = require('body-parser');
const {authorize, ADMIN, LOGGED_USER} = require('../middlewares/auth');
const {listUsers, createUser, replaceUser, updateUser, uploadImage, getImage, deleteImage} = require('../validations/user.validation');
const {UPLOAD_LIMIT} = require('config/vars');

const users = require('express').Router({mergeParams: true});
const tracks = require('./track.route');
const trainees = require('./trainee.route');

const uploadsFolder = path.resolve(`${__dirname}/../../../uploads`);
users.use(bodyParser.urlencoded({extended: true}));

/**
 * Load user when API with userId route parameter is hit
 */
users.param('userId', controller.load);

users
    .route('/')
    /**
     * @api {get} users List Users
     * @apiDescription Get a list of users
     * @apiVersion 1.0.0
     * @apiName ListUsers
     * @apiGroup User
     * @apiPermission admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {Number{1-}}                  [page=1]     List page
     * @apiParam  {Number{1-100}}               [perPage=1]  Users per page
     * @apiParam  {String}                      [name]       User's name
     * @apiParam  {String}                      [email]      User's email
     * @apiParam  {String=trainee,coach,admin}  [role]       User's role
     *
     * @apiSuccess {Object[]} users List of users.
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admins can access the data
     */
    .get(authorize(ADMIN), validate(listUsers), controller.list)
    /**
     * @api {post} users Create User
     * @apiDescription Create a new user
     * @apiVersion 1.0.0
     * @apiName CreateUser
     * @apiGroup User
     * @apiPermission admin
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {String}                      email     User's email
     * @apiParam  {String{6..128}}              password  User's password
     * @apiParam  {String{..128}}               [name]    User's name
     * @apiParam  {String=trainee,coach,admin}  [role]    User's role
     *
     * @apiSuccess (Created 201) {String}  id         User's id
     * @apiSuccess (Created 201) {String}  name       User's name
     * @apiSuccess (Created 201) {String}  email      User's email
     * @apiSuccess (Created 201) {String}  role       User's role
     * @apiSuccess (Created 201) {String}  coach      User's coach
     * @apiSuccess (Created 201) {Date}    createdAt  Timestamp
     *
     * @apiError (Bad Request 400)   ValidationError  Some parameters may contain invalid values
     * @apiError (Unauthorized 401)  Unauthorized     Only authenticated users can create the data
     * @apiError (Forbidden 403)     Forbidden        Only admins can create the data
     */
    .post(authorize(ADMIN), validate(createUser), controller.create);

users
    .route('/profile')
    /**
     * @api {get} users/profile User Profile
     * @apiDescription Get logged in user profile information
     * @apiVersion 1.0.0
     * @apiName UserProfile
     * @apiGroup User
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiSuccess {String}  id         User's id
     * @apiSuccess {String}  name       User's name
     * @apiSuccess {String}  email      User's email
     * @apiSuccess {String}  role       User's role
     * @apiSuccess {String}  coach      User's coach
     * @apiSuccess {Date}    createdAt  Timestamp
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated Users can access the data
     */
    .get(authorize(), controller.loggedIn);

users
    .route('/:userId')
    /**
     * @api {get} users/:id Get User
     * @apiDescription Get user information
     * @apiVersion 1.0.0
     * @apiName GetUser
     * @apiGroup User
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiSuccess {String}  id         User's id
     * @apiSuccess {String}  name       User's name
     * @apiSuccess {String}  email      User's email
     * @apiSuccess {String}  role       User's role
     * @apiSuccess {String}  coach      User's coach
     * @apiSuccess {Date}    createdAt  Timestamp
     *
     * @apiError (Unauthorized 401) Unauthorized Only authenticated users can access the data
     * @apiError (Forbidden 403)    Forbidden    Only user with same id or admins can access the data
     * @apiError (Not Found 404)    NotFound     User does not exist
     */
    .get(authorize(LOGGED_USER), controller.get)
    /**
     * @api {put} users/:id Replace User
     * @apiDescription Replace the whole user document with a new one
     * @apiVersion 1.0.0
     * @apiName ReplaceUser
     * @apiGroup User
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {String}                      email     User's email
     * @apiParam  {String{6..128}}              password  User's password
     * @apiParam  {String{..128}}               [name]    User's name
     * @apiParam  {String=trainee,coach,admin}  [role]    User's role
     * (You must be an admin to change the user's role)
     *
     * @apiSuccess {String}  id         User's id
     * @apiSuccess {String}  name       User's name
     * @apiSuccess {String}  email      User's email
     * @apiSuccess {String}  role       User's role
     * @apiSuccess {String}  coach      User's coach
     * @apiSuccess {Date}    createdAt  Timestamp
     *
     * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
     * @apiError (Unauthorized 401) Unauthorized Only authenticated users can modify the data
     * @apiError (Forbidden 403)    Forbidden    Only user with same id or admins can modify the data
     * @apiError (Not Found 404)    NotFound     User does not exist
     */
    .put(authorize(LOGGED_USER), validate(replaceUser), controller.replace)
    /**
     * @api {patch} users/:id Update User
     * @apiDescription Update some fields of a user document
     * @apiVersion 1.0.0
     * @apiName UpdateUser
     * @apiGroup User
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam  {String}                      email     User's email
     * @apiParam  {String{6..128}}              password  User's password
     * @apiParam  {String{..128}}               [name]    User's name
     * @apiParam  {String=trainee,coach,admin}  [role]    User's role
     * (You must be an admin to change the user's role)
     *
     * @apiSuccess {String}  id         User's id
     * @apiSuccess {String}  name       User's name
     * @apiSuccess {String}  email      User's email
     * @apiSuccess {String}  role       User's role
     * @apiSuccess {String}  role       User's coach
     * @apiSuccess {Date}    createdAt  Timestamp
     *
     * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
     * @apiError (Unauthorized 401) Unauthorized Only authenticated users can modify the data
     * @apiError (Forbidden 403)    Forbidden    Only user with same id or admins can modify the data
     * @apiError (Not Found 404)    NotFound     User does not exist
     */
    .patch(authorize(LOGGED_USER), validate(updateUser), controller.update)
    /**
     * @api {delete} users/:id Delete User
     * @apiDescription Delete a user
     * @apiVersion 1.0.0
     * @apiName DeleteUser
     * @apiGroup User
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiSuccess (No Content 204)  Successfully deleted
     *
     * @apiError (Unauthorized 401) Unauthorized  Only authenticated users can delete the data
     * @apiError (Forbidden 403)    Forbidden     Only user with same id, user's coach and admins can delete the data
     * @apiError (Not Found 404)    NotFound      User does not exist
     */
    .delete(authorize(LOGGED_USER), controller.remove);

/**
 * Upload image route
 */
const multerStorage = multer.diskStorage({
    destination(req: Request, file: any, cb: any) {
        cb(null, uploadsFolder);
    },
    filename(req: any, file: any, cb: any) {
        if (file.mimetype !== "image/png") {
            cb(new Error("Only PNG images are allowed."))
        }
        // fieldname, originalname, mimetype
        cb(null, `${req.params.userId}.png`);
    }
});

const upload = multer({storage: multerStorage, limits: {fileSize: UPLOAD_LIMIT}});

users
    .route('/:userId/image')
    /**
     * @api {get} users/:userId/image Get User image
     * @apiDescription Get User image
     * @apiVersion 1.0.0
     * @apiName GetUserImage
     * @apiGroup User
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiSuccess {File}  image            User's image
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated Users can access the data
     * @apiError (Forbidden 403)    Forbidden     Only user with same id, user's coach and admins can access the image
     * @apiError (Not Found 404)    NotFound      User does not exist
     */
    .get(authorize(LOGGED_USER), validate(getImage), controller.getImage)
    /**
     * @api {post} users/:userId/image Upload user image
     * @apiDescription Upload User image (also replaces the existing image)
     * @apiVersion 1.0.0
     * @apiName UploadUserImage
     * @apiGroup User
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiParam {File}  image            User's image
     *
     * @apiSuccess (No Content 200)  Successfully created
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated Users can access the data
     * @apiError (Forbidden 403)    Forbidden     Only user with same id, user's coach and admins can access the image
     * @apiError (Not Found 404)    NotFound      User does not exist
     */
    .post(authorize(LOGGED_USER), validate(uploadImage), upload.single('file'), controller.upload)
    /**
     * @api {delete} users/:id Delete User
     * @apiDescription Delete a user
     * @apiVersion 1.0.0
     * @apiName DeleteUser
     * @apiGroup User
     * @apiPermission user
     *
     * @apiHeader {String} Authorization  User's access token
     *
     * @apiSuccess (No Content 204)  Successfully deleted
     *
     * @apiError (Unauthorized 401) Unauthorized  Only authenticated users can delete the data
     * @apiError (Forbidden 403)    Forbidden     Only user with same id, user's coach and admins can delete the data
     * @apiError (Not Found 404)    NotFound      User does not exist
     */
    .delete(authorize(LOGGED_USER), validate(deleteImage), controller.deleteImage);

users.use('/:userId/tracks', tracks);
users.use('/:userId/trainees', trainees);
module.exports = users;
