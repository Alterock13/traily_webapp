import httpStatus = require('http-status');

export {};
const mongoose = require('mongoose');
import {transformData, listData} from '../utils/ModelUtils';

const APIError = require('../utils/APIError');

const userTrackSchema = new mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        name: {
            type: String,
            required: true,
            maxlength: 128,
            index: true,
            trim: true
        },
        rating: {
            type: Number,
            default: 0
        },
        duration: Number,
        locations: [],
        distance: Number,
        createdAt: {type: Date, default: Date.now}
    },
    // { timestamps: true }
);
const ALLOWED_FIELDS = ['id', 'user', 'name', 'rating', 'duration', 'locations', 'distance', 'createdAt'];

userTrackSchema.method({
    // query is optional, e.g. to transform data for response but only include certain "fields"
    transform({query = {}}: { query?: any } = {}) {
        // transform every record (only respond allowed fields and "&fields=" in query)
        return transformData(this, query, ALLOWED_FIELDS);
    }
});

userTrackSchema.statics = {
    /**
     * Get track
     *
     * @param {ObjectId} id - The objectId of track.
     * @returns {Promise<User, APIError>}
     */
    async get(id: any) {
        try {
            let track;

            if (mongoose.Types.ObjectId.isValid(id)) {
                track = await this.findById(id).exec();
            }
            if (track) {
                return track;
            }

            throw new APIError({
                message: 'Track does not exist',
                status: httpStatus.NOT_FOUND
            });
        } catch (error) {
            throw error;
        }
    },
    /**
     * List tracks.
     * @returns {Promise<User[]>}
     */
    list({query}: { query: any }) {
        return listData(this, query, ALLOWED_FIELDS);
    },
    /**
     * Return new validation error
     * if error is a mongoose duplicate key error
     *
     * @param {Error} error
     * @returns {Error|APIError}
     */
    checkDuplicateTrackName(error: any) {
        if (error.name === 'MongoError' && error.code === 11000) {
            return new APIError({
                message: 'Validation Error',
                errors: [
                    {
                        field: 'name',
                        location: 'body',
                        messages: ['"name" already exists']
                    }
                ],
                status: httpStatus.CONFLICT,
                isPublic: true,
                stack: error.stack
            });
        }
        return error;
    }
};

const UserTrack = mongoose.model('UserTrack', userTrackSchema);
UserTrack.ALLOWED_FIELDS = ALLOWED_FIELDS;

module.exports = UserTrack;
