export {};
const httpStatus = require('http-status');
const passport = require('passport');
const mongoose = require('mongoose');
const sanitize = require('mongo-sanitize');
import {User} from '../models';

const APIError = require('../utils/APIError');

const ADMIN = 'admin';
const TRAINEE = 'trainee';
const COACH = 'coach';
const LOGGED_USER = '_loggedUser';

import * as Bluebird from 'bluebird';
// declare global {
//   export interface Promise<T> extends Bluebird<T> {}
// }

const handleJWT = (req: any, res: any, next: any, roles: any) => async (err: any, user: any, info: any) => {
    const error = err || info;
    const logIn: any = Bluebird.promisify(sanitize(req.logIn));
    const apiError = new APIError({
        message: error ? error.message : 'Unauthorized',
        status: httpStatus.UNAUTHORIZED,
        stack: error ? error.stack : undefined
    });

    try {
        if (error || !user) {
            throw error;
        }
        await logIn(user, {session: false});
    } catch (e) {
        return next(apiError);
    }

    if (roles === LOGGED_USER || roles === COACH) {
        const userId = sanitize(req.params.userId);
        const requestedUser = mongoose.Types.ObjectId.isValid(userId) ? await User.findById(userId) : undefined;
        if (!requestedUser) {
            apiError.status = httpStatus.NOT_FOUND;
            apiError.message = 'User not found';
            return next(apiError);
        }

        const loggedUser = user;
        const coachId = requestedUser.coach ? requestedUser.coach._id.toString() : undefined;
        if (requestedUser.id !== loggedUser.id && coachId !== loggedUser.id && loggedUser.role !== 'admin') {
            apiError.status = httpStatus.FORBIDDEN;
            apiError.message = 'Only the user, their coach and admins have access to this feature';
            return next(apiError);
        }
    } else if (!roles.includes(user.role)) {
        apiError.status = httpStatus.FORBIDDEN;
        apiError.message = 'Unknown role';
        return next(apiError);
    } else if (err || !user) {
        return next(apiError);
    }

    req.route.meta = req.route.meta || {};
    req.route.meta.user = user;

    return next();
};

exports.ADMIN = ADMIN;
exports.TRAINEE = TRAINEE;
exports.COACH = COACH;
exports.LOGGED_USER = LOGGED_USER;

exports.authorize = (roles = User.roles) => {
    return (req: any, res: any, next: any) => {
        return passport.authenticate('jwt', {session: false}, handleJWT(req, res, next, roles))(req, res, next);
    };
};

exports.oAuth = (service: any) => passport.authenticate(service, {session: false});
