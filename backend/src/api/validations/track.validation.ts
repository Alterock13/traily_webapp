export {};
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

module.exports = {
    // GET /api/users/:userId/tracks
    listTracks: {
        query: {
            page: Joi.number().min(1),
            perPage: Joi.number()
                .min(1)
                .max(100),
            name: Joi.string(),
            rating: Joi.number()
                .min(0)
                .max(5),
            duration: Joi.number(),
            user: Joi.objectId()
        }
    },

    // POST /api/users/:userId/tracks
    createTrack: {
        body: {
            name: Joi.string().max(128)
                .trim()
                .required()
                .trim(),
            duration: Joi.number()
                .required(),
            locations: Joi.array().items(
                Joi.array().items(
                    Joi.number().required(),
                    Joi.number().required()
                )
            )
                .required()
        }
    },

    // PATCH /api/users/:userId/tracks/:trackId
    updateTrack: {
        body: {
            name: Joi.string().max(128)
                .trim(),
            rating: Joi.number()
                .min(0)
                .max(5)
        }
    },

};
