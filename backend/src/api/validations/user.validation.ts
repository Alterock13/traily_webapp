export {};
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
import {User} from '../models';

module.exports = {
    // GET /api/users
    listUsers: {
        query: {
            page: Joi.number().min(1),
            perPage: Joi.number()
                .min(1)
                .max(100),
            name: Joi.string(),
            email: Joi.string(),
            role: Joi.string().valid(User.roles),
            coach: Joi.objectId()
        }
    },

    // POST /api/users
    createUser: {
        body: {
            email: Joi.string()
                .email()
                .required()
                .trim(),
            password: Joi.string()
                .min(6)
                .max(128)
                .required(),
            name: Joi.string().max(128),
            role: Joi.string().valid(User.roles),
            coach: Joi.objectId()
        }
    },

    // PUT /api/users/:userId
    replaceUser: {
        body: {
            email: Joi.string()
                .email()
                .required()
                .trim(),
            password: Joi.string()
                .min(6)
                .max(128)
                .required(),
            name: Joi.string().max(128),
            role: Joi.string().valid(User.roles),
            coach: Joi.objectId()
        },
        params: {
            userId: Joi.objectId()
                .required(),
            coachId: Joi.objectId()
        }
    },

    // PATCH /api/users/:userId
    updateUser: {
        body: {
            email: Joi.string().email()
                .trim(),
            password: Joi.string()
                .min(6)
                .max(128),
            name: Joi.string().max(128),
            role: Joi.string().valid(User.roles),
            coach: Joi.string()
        },
        params: {
            userId: Joi.objectId()
                .required(),
            coachId: Joi.objectId()
        }
    },

    // POST /api/users/:userId/image
    uploadImage: {
        params: {
            userId: Joi.objectId()
                .required()
        }
    },

    // GET /api/users/:userId/image
    getImage: {
        params: {
            userId: Joi.objectId()
                .required()
        }
    },

    // DELETE /api/users/:userId/image
    deleteImage: {
        params: {
            userId: Joi.objectId()
                .required()
        }
    }
};
