export {};
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

module.exports = {
    // POST /api/auth/register
    register: {
        body: {
            email: Joi.string()
                .email()
                .required()
                .trim(),
            password: Joi.string()
                .required()
                .min(6)
                .max(128)
        }
    },

    // POST /api/auth/login
    login: {
        body: {
            email: Joi.string()
                .email()
                .required()
                .trim(),
            password: Joi.string()
                .required()
                .max(128)
        }
    },

    // POST /api/auth/facebook
    // POST /api/auth/google
    oAuth: {
        body: {
            access_token: Joi.string().required()
        }
    },

    // POST /api/auth/refresh
    refresh: {
        body: {
            email: Joi.string()
                .email()
                .required()
                .trim(),
            refreshToken: Joi.string().required()
        }
    },

    // POST /api/auth/logout
    logout: {
        body: {
            userId: Joi.objectId()
                .required()
        }
    },

    // POST /api/auth/forgot-password
    forgotPassword: {
        body: {
            email: Joi.string()
                .email()
                .required()
                .trim()
        }
    }
};
