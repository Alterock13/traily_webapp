export {};
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

module.exports = {
    // GET /api/users/:userId/trainees
    listTrainees: {
        query: {
            page: Joi.number().min(1),
            perPage: Joi.number()
                .min(1)
                .max(100),
            name: Joi.string()
                .trim(),
            email: Joi.string()
                .trim()
        }
    },

    // POST /api/users/:userId/tracks
    assignTrainee: {
        body: {
            email: Joi.string()
                .email()
                .required()
                .trim()
        }
    },

    // PATCH /api/users/:userId/tracks/:trackId
    removeTrainee: {
        body: {
            traineeId: Joi.objectId()
                .required()
                .trim()
        }
    }

};
